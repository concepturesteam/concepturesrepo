<?php 
	$page_id=1;
	include('includes/header.php'); 
?>
    <div class="about_company">
        <div class="container">
            <div class="row">
                <!--<div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="company_slider">
                        <div id="example-20" class="ma5slider anim-vertical vertical-dots vertical-navs center-dots inside-dots autoplay" data-tempo="3500">
                            <div class="slides">
                                <a href="#slide-1"><div class="video_tab">
                    <iframe class="slide_video" src="https://www.youtube.com/embed/SjNo63SSo9o?ecver=2" frameborder="0" allowfullscreen></iframe>
                </div></a>
                            </div>
                        </div>
                    </div>
                </div>-->
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="contant">
<!--                        <h1>This is the</h1>-->
                        <h1 class="bodhi_color">We defines Quality</h1>
                        <p> Bodhi Concepteurs consists of a dynamic, talented and experienced team of professionals experts at customizing architectural designs that stand out in the crowd leaving no barriers of half-baked premises. With a badge of unprecedented legacy in serving the engineering and architecture industry for more than 5 years now, we have our base located in Calicut tending to a loyal base of conventional clientele all over the world now.</p> <a href="about.php">Read more</a> </div>
                </div>
            </div>
        </div>
    </div>
    <div class="our_skills">
        <div class="container">
            <h1>APPLICATION</h1> <span><h1 class="bodhi_color">Our Services</h1></span> </div>
        <div class="skill_slider">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <!-- slider 1 -->
                        <ul class="bxslider icon_slid slide_of_skills">
                            <li><span><img src="images/skill_icon.png" alt=""></span>
                                <h2>Floor plan Analysis</h2> </li>
                            <li><span><img src="images/skill_icon.png" alt=""></span>
                                <h2>Customised Interiors  </h2> </li>
<!--
                            <li><span><img src="images/skill_icon.png" alt=""></span>
                                <h2>lorem ipsum </h2> </li>
-->
                        </ul>
                    </div>
                    <div class="col-lg-9 col-md-9">
                        <!-- slider 2 -->
                        <ul class="bxslider skill_about slide_of_skills">
                            <a href="services.php"><li>
                                <p><b>Floor plan Analysis</b>
                                    <br>We provide this services without charge. Bodhi Concepteurs is happy to assist you with your plan at any stage.</p>
                            </li></a>
                            <a href="services.php"><li>
                                <p><b>Customised Interiors</b>
                                    <br>We rely on our professional knowledge and advanced experience in the field to walk our client through every process of styling the building with exceptional customer service. </p>
                            </li></a>
<!--
                            <li>
                                <p><b>This is the lorem ipsum main</b>
                                    <br> Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an pericula euripidis. Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an pericula euripidis. Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an pericula euripidis</p>
                            </li>
-->
                        </ul>
                    </div>
                </div>
                <!-- custom controls -->
                <div class="bxslider-controls"> <a class="pull-left" href="#"><span><i class="fa fa-chevron-left" aria-hidden="true"></i></span></a> <a class="pull-right" href="#"><span><i class="fa fa-chevron-right" aria-hidden="true"></i></span></a> </div>
                 </div>
        </div>
    </div>
    <div class="projects">
        <div class="project_slid">
            <ul class="bxslider">
                <li style="background:url('images/2.jpg') center; background-size:cover;">
                    <div class="blur"></div>
                    <div class="container">
                        <div class="wrap">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <a href="gafoor_home.php"><div class="pro_image"> <img src="images/2.jpg" alt=""> </div></a>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="pro_details">
                                        <h1>Residential</h1> <span>GAFOOR THIKKODI</span>
                                        <hr>
                                        <p>Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an pericula euripidis. Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an pericula euripidis. Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an pericula euripidis</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li style="background:url('images/3.jpg') center; background-size:cover;">
                    <div class="blur"></div>
                    <div class="container">
                        <div class="wrap">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <a href="#"><div class="pro_image"> <img src="images/3.jpg" alt=""> </div></a>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="pro_details">
                                        <h1>lorem ipsum</h1> <span>NAVAS KODIKKAL</span>
                                        <hr>
                                        <p>Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an pericula euripidis. Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an pericula euripidis. Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an pericula euripidis</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li style="background:url('images/1.jpg') center; background-size:cover;">
                    <div class="blur"></div>
                    <div class="container">
                        <div class="wrap">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <a href="bangalore_lumion.php"><div class="pro_image"> <img src="images/1.jpg" alt=""> </div></a>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="pro_details">
                                        <h1>Commercial</h1> <span>Bangalore Lumion</span>
                                        <hr>
                                        <p>Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an pericula euripidis. Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an pericula euripidis. Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an pericula euripidis</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <!--<li style="background:url('images/Layer-9.jpg') center; background-size:cover;">
                    <div class="blur"></div>
                    <div class="container">
                        <div class="wrap">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="pro_image"> <img src="images/Layer-9.jpg" alt=""> </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="pro_details">
                                        <h1>lorem ipsum</h1> <span>phaedrum torquatos nec</span>
                                        <hr>
                                        <p>Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an pericula euripidis. Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an pericula euripidis. Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an pericula euripidis</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>-->
            </ul>
        </div>
    </div>

<div class="testimonial">
    <div class="container">
        
<!--        <span><h1>Testimonials</h1></span>-->
        
<!--
<div class="carousel-reviews broun-block">
    <div class="container">
        <div class="row">
            <div id="carousel-reviews" class="carousel slide" data-ride="carousel">
            
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="col-md-6 col-sm-6">
        				    <div class="block-text rel zmin">
						        <p>Never before has there been a good film portrayal of ancient Greece's favourite myth. So why would Hollywood start now? This latest attempt at bringing the son of Zeus to the big screen is brought to us by X-Men: The last Stand director Brett Ratner. If the name of the director wasn't enough to dissuade ...</p>
							    <ins class="ab zmin sprite sprite-i-triangle block"></ins>
                                
					        </div>
							<div class="person-text rel">
								<img alt="" src="http://myinstantcms.ru/images/img13.png">
								<a title="" href="#">Anna</a>
								<i>from Glasgow, Scotland</i>
							</div>
						</div>
            			<div class="col-md-6 col-sm-6 hidden-xs">
						    <div class="block-text rel zmin">
        						<p>The 2013 movie "The Purge" left a bad taste in all of our mouths as nothing more than a pseudo-slasher with a hamfisted plot, poor pacing, and a desperate attempt at "horror." Upon seeing the first trailer for "The Purge: Anarchy," my first and most immediate thought was "we really don't need another one of these."  </p>
					            <ins class="ab zmin sprite sprite-i-triangle block"></ins>
				            </div>
							<div class="person-text rel">
								<img alt="" src="http://myinstantcms.ru/images/img14.png">
						        <a title="" href="#">Ella Mentree</a>
								<i>United States</i>
							</div>
						</div>
                    </div> 
                    <div class="item">
                        <div class="col-md-6 col-sm-6">
        				    <div class="block-text rel zmin">
						        <p>Never before has there been a good film portrayal of ancient Greece's favourite myth. So why would Hollywood start now? This latest attempt at bringing the son of Zeus to the big screen is brought to us by X-Men: The last Stand director Brett Ratner. If the name of the director wasn't enough to dissuade ...</p>
							    <ins class="ab zmin sprite sprite-i-triangle block"></ins>
					        </div>
							<div class="person-text rel">
								<img alt="" src="http://myinstantcms.ru/images/img13.png">
								<a title="" href="#">Anna</a>
								<i>from Glasgow, Scotland</i>
							</div>
						</div>
            			<div class="col-md-6 col-sm-6 hidden-xs">
						    <div class="block-text rel zmin">
        						<p>The 2013 movie "The Purge" left a bad taste in all of our mouths as nothing more than a pseudo-slasher with a hamfisted plot, poor pacing, and a desperate attempt at "horror." Upon seeing the first trailer for "The Purge: Anarchy," my first and most immediate thought was "we really don't need another one of these."  </p>
					            <ins class="ab zmin sprite sprite-i-triangle block"></ins>
				            </div>
							<div class="person-text rel">
								<img alt="" src="http://myinstantcms.ru/images/img14.png">
						        <a title="" href="#">Ella Mentree</a>
								<i>United States</i>
							</div>
						</div>
                    </div>  
                    
                </div>
                <a class="left carousel-control" href="#carousel-reviews" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-reviews" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        
        
    </div>
</div>
        </div>
-->
    </div>
</div>

<div class="clientele">
    <div class="container">
<div class="col-lg-2 col-md-2 col-sm-3">
    <h2>Clientele</h2>
</div>
<div class="col-lg-10 col-md-10 col-sm-9  back_gery">
    
    
    <div class="client_slid">
            <ul class="bxslider_carousel">
                <li><img src="images/logo-b.jpg" alt="Bodhi" /></li>
                <li><img src="images/logo-c.png" alt="Bodhi" /></li>
                <li><img src="images/logo-d.jpg" alt="Bodhi" /></li>
            </ul>
        </div>
    

</div>
    </div>
</div>
<hr class="green">
<div class="sisters">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="bodhi_info">
                    <h3>OUR SISTER CONCERN</h3>
                    <img src="images/bodhi.png" alt="Bodhi Info Solutions Pvt Ltd">
                    <h1>BODHI INFO SOLUTIONS PVT LTD</h1>
                    <p>Bodhi is a culmination of creative young minds &amp; experienced professionals, who strive hard to deliver their best. We follow a client-centric approach, where in we believe that a measure of our success is only as much as your success.</p>
                    <a class="btn pmd-ripple-effect btn-success" href="http://bodhiinfo.com/" target="_blank">VISIT WEBSITE</a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="lapic">
                    <h1>We are Expertise in<br>
                        <b>Structural Designing</b></h1>
                    <img src="images/lapic.png" alt="Lapic">
                    <a class="btn pmd-ripple-effect btn-success" href="#">VISIT WEBSITE</a>
                </div>
            </div>
        </div>
    </div>
</div>


    <?php include('includes/footer.php');?>
        <script>
            $(document).ready(function(){
                $('.carousel[data-type="multi"] .item').each(function(){
                  var next = $(this).next();
                  if (!next.length) {
                    next = $(this).siblings(':first');
                  }
                  next.children(':first-child').clone().appendTo($(this));

                  for (var i=0;i<4;i++) {
                    next=next.next();
                    if (!next.length) {
                        next = $(this).siblings(':first');
                    }

                    next.children(':first-child').clone().appendTo($(this));
                  }
                });
            });
            $(window).on('load', function () {
                $('.ma5slider').ma5slider({
                    autoplayTime: 8e5

                });
            });
            $(document).ready(function () {
                $('.bxslider').bxSlider({
                    pager: false,
                    auto: true,
                    autoControls: true
                });
                $('.bxslider_carousel').bxSlider({
                      auto: true,
                      pager: false,
                    controls: false,
                    minSlides: 1,
                    maxSlides: 6,
                    moveSlides: 1,
                      slideWidth: 150,
                      slideMargin: 40
                });
                $('.bxslider_offers').bxSlider({
                      auto: true,
                      pager: false,
                    controls: false,
                    minSlides: 1,
                    maxSlides: 3,
                    moveSlides: 1,
                      slideWidth: 350,
                      slideMargin: 10
                });
            });
        </script>
        </body>

        </html>
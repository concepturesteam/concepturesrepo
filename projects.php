<?php 
	$page_id=4;
	include('includes/header.php'); 
?>
<div class="inner_layout">
    <div class="about_company">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="content">
                        <div class="subtitle">PORTFOLIO</div> 
                        <h2 class="title">Presenting Our Works</h2> 
                        <a href="live-projects.php" class="prague-services-link a-btn-2 creative anima"> <span class="a-btn-line"></span> Live Projects </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="projects_section">
        <div class="container">
            <!--Scrollable tab example -->
<div class="pmd-card pmd-z-depth"> 
	<div class="pmd-tabs pmd-tabs-scroll">
		<div class="pmd-tabs-scroll-left"><i class="material-icons pmd-sm">chevron_left</i></div>
		<div class="pmd-tabs-scroll-container pmd-z-depth" style="cursor: grab;">
			<div class="pmd-tab-active-bar"></div>
			<ul class="nav nav-tabs nav-justified" role="tablist">
				<li role="presentation" class="active"><a href="#home-scrollable" aria-controls="about" role="tab" data-toggle="tab">Residential</a></li>
				<li role="presentation"><a href="#commec-scrollable" aria-controls="work" role="tab" data-toggle="tab">Commercial</a></li>
				<li role="presentation"><a href="#facelift-scrollable" aria-controls="work" role="tab" data-toggle="tab">Facelift</a></li>
				<li role="presentation"><a href="#intior-scrollable" aria-controls="work" role="tab" data-toggle="tab">Interior</a></li>
				<li role="presentation"><a href="#religi-scrollable" aria-controls="work" role="tab" data-toggle="tab">Religious</a></li>
				<li role="presentation"><a href="#Hospitalty-scrollable" aria-controls="work" role="tab" data-toggle="tab">Hospitality</a></li>
			</ul>
		</div>
		<div class="pmd-tabs-scroll-right"><i class="material-icons pmd-sm">chevron_right</i></div>	
	</div>
	<div class="pmd-card-body">
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="home-scrollable">
                <div class="vc_row wpb_row vc_row-fluid  margin-lg-30t margin-lg-105b margin-sm-20t margin-sm-30b"><div class="wpb_column vc_column_container vc_col-sm-12 "><div class="vc_column-inner "><div class="wpb_wrapper"><div data-unique-key="653bde77265ebcc57a08b72449759fd7"><div class="row prague_exhibition_grid prague_count_col3 prague_gap_col15 no-footer-content prague-load-wrapper" data-columns="prague_count_col3" data-gap="prague_gap_col15" style="position: relative;">
                
                <div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_1">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="bangalore_lumion.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/1.jpg);">
<img width="2000" height="1335" src="images/1.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Bangalore</h4>
<div class="exhib-grid-item-category"></div>
</div>
</a>
</div>
</div>
</div>
                
                <div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_2" >
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="gafoor_home.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/2.jpg);">
<img width="2000" height="1335" src="images/2.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Gafoor</h4>
<div class="exhib-grid-item-category">Thikkodi</div>
</div>
</a>
</div>
</div>
</div>
                    <div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_3" >
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="hameed_villa.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/4.jpg);">
<img width="2000" height="1335" src="images/4.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Hameed Villa</h4>
<div class="exhib-grid-item-category">Paris, France</div>
</div>
</a>
</div>
</div>
</div>
</div>
</div>
</div></div></div></div>
                <div class="vc_row wpb_row vc_row-fluid  margin-lg-30t margin-lg-105b margin-sm-20t margin-sm-30b"><div class="wpb_column vc_column_container vc_col-sm-12 "><div class="vc_column-inner "><div class="wpb_wrapper"><div data-unique-key="653bde77265ebcc57a08b72449759fd7"><div class="row prague_exhibition_grid prague_count_col3 prague_gap_col15 no-footer-content prague-load-wrapper" data-columns="prague_count_col3" data-gap="prague_gap_col15" style="position: relative;">
                
                <div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_1">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="siras-nandi.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/siras_nandi.jpg);">
<img width="2000" height="1335" src="images/siras_nandi.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Siras Nandi</h4>
<div class="exhib-grid-item-category"></div>
</div>
</a>
</div>
</div>
</div>
                    <div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_2">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="aslam-kuruvangad.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/projects/aslam.jpg);">
<img width="2000" height="1335" src="images/projects/aslam.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Aslam</h4>
<div class="exhib-grid-item-category">KURUVANGAD </div>
</div>
</a>
</div>
</div>
</div>
                    <div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_3">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="noufal_thikkodi.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/projects/noufak_thikodi_2.jpg);">
<img width="2000" height="1335" src="images/projects/noufak_thikodi_2.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/noufak_thikodi_2.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Noufal</h4>
<div class="exhib-grid-item-category">Thikkodi</div>
</div>
</a>
</div>
</div>
</div>
</div>
</div>
</div></div></div></div>
                <div class="vc_row wpb_row vc_row-fluid  margin-lg-30t margin-lg-105b margin-sm-20t margin-sm-30b"><div class="wpb_column vc_column_container vc_col-sm-12 "><div class="vc_column-inner "><div class="wpb_wrapper"><div data-unique-key="653bde77265ebcc57a08b72449759fd7"><div class="row prague_exhibition_grid prague_count_col3 prague_gap_col15 no-footer-content prague-load-wrapper" data-columns="prague_count_col3" data-gap="prague_gap_col15" style="position: relative;">
                
                <!--<div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_1">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="koya.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/projects/koya_2.jpg);">
<img width="2000" height="1335" src="images/projects/koya_2.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Koya</h4>
<div class="exhib-grid-item-category"></div>
</div>
</a>
</div>
</div>
</div>-->
                    <div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_1">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="thaha.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/projects/thaha_1.jpg);">
<img width="2000" height="1335" src="images/projects/thaha_1.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Thaha</h4>
<div class="exhib-grid-item-category"></div>
</div>
</a>
</div>
</div>
</div>
            <!--        <div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_2">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="aslam-kuruvangad.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/projects/aslam.jpg);">
<img width="2000" height="1335" src="images/projects/aslam.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Aslam</h4>
<div class="exhib-grid-item-category">KURUVANGAD </div>
</div>
</a>
</div>
</div>
</div>
                    <div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_3">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="noufal_thikkodi.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/projects/noufak_thikodi_2.jpg);">
<img width="2000" height="1335" src="images/projects/noufak_thikodi_2.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/noufak_thikodi_2.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Noufal</h4>
<div class="exhib-grid-item-category">Thikkodi</div>
</div>
</a>
</div>
</div>
</div>-->
</div>
</div>
</div></div></div></div>
            </div>
            
            <div role="tabpanel" class="tab-pane" id="commec-scrollable"><div class="row prague_exhibition_grid prague_count_col3 prague_gap_col15 no-footer-content prague-load-wrapper" data-columns="prague_count_col3" data-gap="prague_gap_col15" style="position: relative;">
                
                <div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_1">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="pharmsy.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/6.jpg);">
<img width="2000" height="1335" src="images/6.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Pharmsi</h4>
<div class="exhib-grid-item-category">Kozhikode</div>
</div>
</a>
</div>
</div>
</div>
                
                <div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_2">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="sidco.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/7.jpg);">
<img width="2000" height="1335" src="images/7.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">SIDCO</h4>
<div class="exhib-grid-item-category">Vengara</div>
</div>
</a>
</div>
</div>
</div>
                <div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_3">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="olive-garden.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/projects/olive_1.jpg);">
<img width="2000" height="1335" src="images/projects/olive_1.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Olive Garden</h4>
<div class="exhib-grid-item-category">Banglore</div>
</div>
</a>
</div>
</div>
</div>
                
</div>
                <div class="row prague_exhibition_grid prague_count_col3 prague_gap_col15 no-footer-content prague-load-wrapper" data-columns="prague_count_col3" data-gap="prague_gap_col15" style="position: relative;">
                
                <div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_1">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="shope-payyoli.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/projects/shop_1.jpg);">
<img width="2000" height="1335" src="images/projects/shop_1.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Shop</h4>
<div class="exhib-grid-item-category">Payyoli</div>
</div>
</a>
</div>
</div>
</div>
                
                <!--<div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_2">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="sidco.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/7.jpg);">
<img width="2000" height="1335" src="images/7.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">SIDCO</h4>
<div class="exhib-grid-item-category">Vengara</div>
</div>
</a>
</div>
</div>
</div>-->
                <!--<div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_3">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="olive-garden.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/projects/olive_1.jpg);">
<img width="2000" height="1335" src="images/projects/olive_1.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Olive Garden</h4>
<div class="exhib-grid-item-category">Banglore</div>
</div>
</a>
</div>
</div>
</div>-->
                
</div>
            
            </div>
            
            <div role="tabpanel" class="tab-pane" id="intior-scrollable"><div class="row prague_exhibition_grid prague_count_col3 prague_gap_col15 no-footer-content prague-load-wrapper" data-columns="prague_count_col3" data-gap="prague_gap_col15" style="position: relative;">
                
                <div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_1">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="sameer-vallikulangara.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/sameer_vallikulangara.jpg);">
<img width="2000" height="1335" src="images/sameer_vallikulangara.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Sameer </h4>
<div class="exhib-grid-item-category">Vallikulangara</div>
</div>
</a>
</div>
</div>
</div>
                
                <div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_2">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="ahammad_nandi.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/ahammad_nandi.jpg);">
<img width="2000" height="1335" src="images/ahammad_nandi.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Ahammad Nandi</h4>
<div class="exhib-grid-item-category"></div>
</div>
</a>
</div>
</div>
</div>
                
<div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_3">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="gafoor_vengara.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/gafoor_vengara.jpg);">
<img width="2000" height="1335" src="images/gafoor_vengara.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Gafoor</h4>
<div class="exhib-grid-item-category">Vengara</div>
</div>
</a>
</div>
</div>
</div>
</div>
            <div class="row prague_exhibition_grid prague_count_col3 prague_gap_col15 no-footer-content prague-load-wrapper" data-columns="prague_count_col3" data-gap="prague_gap_col15" style="position: relative;">
                
                <div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_1">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="dr-farhana.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/dr_farhana.jpg);">
<img width="2000" height="1335" src="images/dr_farhana.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Dr Farhana</h4>
<div class="exhib-grid-item-category">Koothuparamba</div>
</div>
</a>
</div>
</div>
</div>
           <div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_2">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="shaji-payyoli.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/projects/24-1-13-1.jpg);">
<img width="2000" height="1335" src="images/projects/24-1-13-1.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Shaji</h4>
<div class="exhib-grid-item-category">Payyoli</div>
</div>
</a>
</div>
</div>
</div>   
                <div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_3">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="navas-kodikkal.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/projects/navas_1.jpg);">
<img width="2000" height="1335" src="images/projects/navas_1.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Navas</h4>
<div class="exhib-grid-item-category">Kodikkal</div>
</div>
</a>
</div>
</div>
</div>   
                
</div>
                
                <div class="row prague_exhibition_grid prague_count_col3 prague_gap_col15 no-footer-content prague-load-wrapper" data-columns="prague_count_col3" data-gap="prague_gap_col15" style="position: relative;">
                
                <!--<div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_1">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="thaha.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/projects/thaha_1.jpg);">
<img width="2000" height="1335" src="images/projects/thaha_1.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Thaha</h4>
<div class="exhib-grid-item-category"></div>
</div>
</a>
</div>
</div>
</div>-->
           <!--<div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_2">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="shaji-payyoli.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/projects/24-1-13-1.jpg);">
<img width="2000" height="1335" src="images/projects/24-1-13-1.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Shaji</h4>
<div class="exhib-grid-item-category">Payyoli</div>
</div>
</a>
</div>
</div>
</div>   
                <div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_3">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="navas-kodikkal.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/projects/navas_1.jpg);">
<img width="2000" height="1335" src="images/projects/navas_1.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Navas</h4>
<div class="exhib-grid-item-category">Kodikkal</div>
</div>
</a>
</div>
</div>
</div> -->  
                
</div>
                
                
            </div>
            
			<div role="tabpanel" class="tab-pane" id="religi-scrollable"><div class="vc_row wpb_row vc_row-fluid  margin-lg-30t margin-lg-105b margin-sm-20t margin-sm-30b"><div class="wpb_column vc_column_container vc_col-sm-12 "><div class="vc_column-inner "><div class="wpb_wrapper"><div data-unique-key="653bde77265ebcc57a08b72449759fd7"><div class="row prague_exhibition_grid prague_count_col3 prague_gap_col15 no-footer-content prague-load-wrapper" data-columns="prague_count_col3" data-gap="prague_gap_col15" style="position: relative;">
<div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_1" style="position: absolute; left: 0px; top: 0px;">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="nettur_masjid.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/projects/nettur_1.jpg);">
<img width="2000" height="1335" src="images/projects/nettur_1.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Masjid</h4>
<div class="exhib-grid-item-category">Nettur</div>
</div>
</a>
</div>
</div>
</div>
                
                <div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_2" style="position: absolute; left: 380px; top: 0px;">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="chengottu_kav_masjid.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/5.jpg);">
<img width="2000" height="1335" src="images/projects/5.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Masjid</h4>
<div class="exhib-grid-item-category">Chengottu Kavu</div>
</div>
</a>
</div>
</div>
</div>
                
                <div class="portfolio-item-wrapp prague_filter_class  p_f_01e03f4 prague_filter_class_3" style="position: absolute; right: 0px; top: 0px;">
<div class="portfolio-item">
<div class="prague-exhib-grid-wrapper">
<a class="exhib-grid-item-link" href="pallimukku-masjid.php">
<div class="exhib-grid-item-img s-back-switch" style="background-image: url(images/projects/pllimukk_1.jpg);">
<img width="2000" height="1335" src="images/projects/pllimukk_1.jpg" class="s-img-switch wp-post-image" alt="" srcset="images/88078a41049671.579736c352fcb.jpg" sizes="(max-width: 2000px) 100vw, 2000px" style="display: none;"> </div>
<div class="exhib-grid-item-content">
<h4 class="exhib-grid-item-title">Masjid</h4>
<div class="exhib-grid-item-category">Pallimukku</div>
</div>
</a>
</div>
</div>
</div>
</div>
                
</div>
</div></div></div></div></div>
		</div>
	</div>
</div> <!--Scrollable tab example end-->
        </div>
    </div>
</div>


    <?php include('includes/footer.php');?>
        <script>
            $(document).ready(function(){
                $('.carousel[data-type="multi"] .item').each(function(){
                  var next = $(this).next();
                  if (!next.length) {
                    next = $(this).siblings(':first');
                  }
                  next.children(':first-child').clone().appendTo($(this));

                  for (var i=0;i<4;i++) {
                    next=next.next();
                    if (!next.length) {
                        next = $(this).siblings(':first');
                    }

                    next.children(':first-child').clone().appendTo($(this));
                  }
                });
            });
            $(window).on('load', function () {
                $('.ma5slider').ma5slider({
                    autoplayTime: 8e5

                });
            });
            $(document).ready(function () {
                $('.bxslider').bxSlider({
                    pager: false,
                    auto: true,
                    autoControls: true
                });
                $('.bxslider_carousel').bxSlider({
                      auto: true,
                      pager: false,
                    minSlides: 1,
                    maxSlides: 6,
                    moveSlides: 1,
                      slideWidth: 150,
                      slideMargin: 10
                });
            });
        </script>
<!-- Propeller tabs js -->
<script type="text/javascript" language="javascript" src="http://propeller.in/components/tab/js/tab-scrollable.js"></script>
<script type="text/javascript">
	$(document).ready( function() {
		$('.pmd-tabs').pmdTab();
	});
</script>
        </body>

        </html>
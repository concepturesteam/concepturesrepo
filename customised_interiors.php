<?php 
	$page_id=3;
	include('includes/header.php'); 
?>
<div class="inner_layout">
    <div class="about_company">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="contant">
                        <h1>Customised Interiors</h1>
                        <!--<h1 class="bodhi_color">BODHI CONCEPTEURS</h1>-->
                        <p> We rely on our professional knowledge and advanced experience in the field to walk our client through every process of styling the building with exceptional customer service. We understand the project we begin has specific needs, budgets and a level of quality with the work involved. We design always with a concept that directly correlates to these needs of clients and your individual needs. You can choose the colur , textures and materials for you dream projects<br><br>
                        Our interior environments are all custom solutions, planned to maximize operational efficiency through space layout and technology. We deliver imaginative work environments, amenities and visual imagery that attract the best talent. Our online proprietary tools helps you to customize almost every aspect of your home. You can choose from the wide range of furniture models we have and can install within 45 days of booking.
<br><br>
Our in house consultant will guide you to choose right decision or you can ask to produce a different theme for your home exclusively.

                        </p> </div>
                </div>
            </div>
        </div>
    </div>
    <div class="projects_section cusom_intrio">
        <div class="container">
            <!--<div class="caption">
                <h1>All Services</h1>
            </div>-->
            <div class="service_row row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="image_bx">
                        <img src="images/p1_c.jpg" alt="Bodhi Concepteurs">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="content">
                        
                        <h2>KITCHEN</h2>
                        <p>
Fully Loaded Overhead and Bottom Storage Spaces<br><br>
( 360 X 60cm - 360 X 90 cm)<br><br>
Hettich (German Made - 15 year warranty) Accessories-9 No's<br><br>
Hood and Hob - Faber - Italy Made</p>
                    </div>
                </div>
            </div>
            <div class="service_row row">
                
                <div class="col-lg-6 col-md-6 col-sm-6 col-sm-push-6">
                    <div class="image_bx">
                        <img src="images/p1_c.jpg" alt="Bodhi Concepteurs">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-sm-pull-6">
                    <div class="content">
                        <h2>KITCHEN</h2>
                        <p>
Fully Loaded Overhead and Bottom Storage Spaces<br><br>
( 360 X 60cm - 360 X 90 cm)<br><br>
Hettich (German Made - 15 year warranty) Accessories-9 No's<br><br>
Hood and Hob - Faber - Italy Made</p>
                    </div>
                </div>
            </div>
            
        </div>
    </div>

    <?php include('includes/footer.php');?>
        <script>
            $(document).ready(function(){
                $('.carousel[data-type="multi"] .item').each(function(){
                  var next = $(this).next();
                  if (!next.length) {
                    next = $(this).siblings(':first');
                  }
                  next.children(':first-child').clone().appendTo($(this));

                  for (var i=0;i<4;i++) {
                    next=next.next();
                    if (!next.length) {
                        next = $(this).siblings(':first');
                    }

                    next.children(':first-child').clone().appendTo($(this));
                  }
                });
            });
            $(window).on('load', function () {
                $('.ma5slider').ma5slider({
                    autoplayTime: 8e5

                });
            });
            $(document).ready(function () {
                $('.bxslider').bxSlider({
                    pager: false,
                    auto: true,
                    autoControls: true
                });
                $('.bxslider_carousel').bxSlider({
                      auto: true,
                      pager: false,
                    minSlides: 1,
                    maxSlides: 6,
                    moveSlides: 1,
                      slideWidth: 150,
                      slideMargin: 10
                });
            });
        </script>
        </body>

        </html>

        <!-- footer begin -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <img src="images/logo.png" class="logo-small" alt=""><br>
                        We are team based in Calicut. Our expertise on Interior Design. An interior design firm driven from highly imaginative design solutions that satisfy human, physical, technical, aesthetic and functional needs. We’ll work with your tastes, preferences, and space to create your ideal environment.
                    </div>

                    <!--<div class="col-md-4">
                        <div class="widget widget_recent_post">
                            <h3>Latest News</h3>
                            <ul>
                                <li><a href="#">5 Things That Take a Room from Good to Great</a></li>
                                <li><a href="#">Functional and Stylish Wall-to-Wall Shelves</a></li>
                                <li><a href="#">9 Unique and Unusual Ways to Display Your TV</a></li>
                                <li><a href="#">The 5 Secrets to Pulling Off Simple, Minimal Design</a></li>
                                <li><a href="#">How to Make a Huge Impact With Multiples</a></li>
                            </ul>
                        </div>
                    </div>-->

                    <div class="col-md-6">
                        <h3>Contact Us</h3>
                        <div class="widget widget-address">
                            <address>
                                <span>3rd Floor, CD Tower
Opp. Baby Memorial Hospital
Calicut</span>
                                <span><strong>Phone:</strong>+91 7560940007</span>
                                <span><strong>Fax:</strong>+91 7560940007</span>
                                <span><strong>Email:</strong><a href="mailto:mail@designsbodhi.com">mail@designsbodhi.com</a></span>
                                <span><strong>Web:</strong><a href="#">www.designsbodhi.com</a></span>
                            </address>
                        </div>
                    </div>
                </div>
            </div>

            <div class="subfooter">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            &copy; 2017 Copyright Bodhi Concepteurs All Rights Reserved     
<!--                            &copy; Copyright 2017 - Archi Interior Design Template by <span class="id-color">Designesia</span>                     -->
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="social-icons">
                                <a href="https://www.facebook.com/BodhiConcepteurs/"><i class="fa fa-facebook fa-lg"></i></a>
                                <a href="#"><i class="fa fa-twitter fa-lg"></i></a>
<!--                                <a href="#"><i class="fa fa-rss fa-lg"></i></a>-->
                                <a href="#"><i class="fa fa-google-plus fa-lg"></i></a>
<!--                                <a href="#"><i class="fa fa-skype fa-lg"></i></a>-->
<!--                                <a href="#"><i class="fa fa-dribbble fa-lg"></i></a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" id="back-to-top"></a>
        </footer>
        <!-- footer close -->
    </div>

<script>    //  analytics code   designbodhigmail
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-99257243-1', 'auto');
  ga('send', 'pageview');

</script>
    <!-- Javascript Files
    ================================================== -->
    <script src="js/jquery.min.js"></script>
    <script src="js/jpreLoader.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/easing.js"></script>
    <script src="js/jquery.scrollto.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/jquery.countTo.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/validation.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3"></script>
    <script src="js/map.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/enquire.min.js"></script>
    <script src="js/designesia.js"></script>
    <script src="demo/demo.js"></script>


    <!-- SLIDER REVOLUTION SCRIPTS  -->
    <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>


</body>

<!-- Mirrored from www.themenesia.com/themeforest/archi/index-slider-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 21 Apr 2017 11:02:26 GMT -->
</html>
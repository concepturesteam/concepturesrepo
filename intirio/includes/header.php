<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from www.themenesia.com/themeforest/archi/index-slider-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 21 Apr 2017 11:01:07 GMT -->
<head>
    <meta charset="utf-8">
    <title>Bodhi Interiors</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta name="description" content="Bodhi Concepteurs is a construction company which  specializes in Architecture, Structural Engineering, 
	Property Management services, Landscape design, Home automation, Floor plan & elevation.">
	<meta name="Keywords" content="Architecture, Structural Engineering, Property Management services, Landscape design, Home automation, Floor plan planner, Elevation, Home design" />
    <meta name="author" content="">
	
	<meta property="og:type" content="Website" />
    <meta property="og:url" content="http://designsbodhi.com/" />
    <meta property="og:title" content="Bodhi Concepteurs : Building contractors" />
    <meta property="og:description" content="Bodhi Concepteurs is a construction company which  specializes in Architecture, Structural Engineering, 
	Property Management services, Landscape design, Home automation, Floor plan & elevation." />
    <meta property="og:image" content="http://designsbodhi.com/images/og-img.jpg" />

    <!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<![endif]-->


    <!-- CSS Files
    ================================================== -->
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css">
   <!-- <link rel="stylesheet" href="css/jpreloader.css" type="text/css">-->
    <link rel="stylesheet" href="css/animate.css" type="text/css">
    <link rel="stylesheet" href="css/plugin.css" type="text/css">
   <!-- <link rel="stylesheet" href="css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="css/owl.theme.css" type="text/css">-->
    <link rel="stylesheet" href="css/owl.transitions.css" type="text/css">
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <link rel="stylesheet" href="demo/demo.css" type="text/css">

    <!-- custom background -->
    <link rel="stylesheet" href="css/bg.css" type="text/css">

    <!-- color scheme -->
    <link rel="stylesheet" href="css/color.css" type="text/css" id="colors">


    <!-- load fonts -->
    <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.css" type="text/css">
    <link rel="stylesheet" href="fonts/elegant_font/HTML_CSS/style.css" type="text/css">
    <link rel="stylesheet" href="fonts/et-line-font/style.css" type="text/css">

    <!-- revolution slider -->
    <link rel="stylesheet" href="rs-plugin/css/settings.css" type="text/css">
    <link rel="stylesheet" href="css/rev-settings.css" type="text/css">
</head>

<body id="homepage">

    <div id="wrapper">

        <!-- header begin -->
        <header class="transparent">
            <div class="info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col">Working Hours Monday - Friday <span class="id-color"><strong>08:00-16:00</strong></span></div>
                            <div class="col">Toll Free <span class="id-color"><strong>1800.899.900</strong></span></div>
                            <!-- social icons -->
                            <div class="col social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-rss"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-envelope-o"></i></a>
                            </div>
                            <!-- social icons close -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- logo begin -->
                        <div id="logo">
                            <a href="../index.php">
                                <img class="logo" src="images/logo.png" alt="">
                            </a>
                        </div>
                        <!-- logo close -->

                        <!-- small button begin -->
                        <span id="menu-btn"></span>
                        <!-- small button close -->
                        
						<!-- mainmenu begin -->
                        <nav>
                            <ul id="mainmenu">
                                <li><a <?php if($page_id==1){ echo 'class="active"'; } ?> href="index.php">Home</a></li>
                                <li><a <?php if($page_id==2){ echo 'class="active"'; } ?> href="offers.php">Offers</a></li>
                                <li><a <?php if($page_id==3){ echo 'class="active"'; } ?> href="projects.php">Products</a></li>
                                <li><a <?php if($page_id==4){ echo 'class="active"'; } ?> href="about.php">About Us</a></li>
                                <li><a <?php if($page_id==5){ echo 'class="active"'; } ?> href="contact.php">Contact</a></li>
                            </ul>
                        </nav>

                    </div>
                    <!-- mainmenu close -->

                </div>
            </div>
        </header>
        <!-- header close -->
    <?php if($page_id==2 || $page_id==3 || $page_id==4 || $page_id==5 || $page_id==6 || $page_id==7 || $page_id==8 || $page_id==9 || $page_id==10 || $page_id==11 || $page_id==12){ ?>
            
    
    <?php } ?>
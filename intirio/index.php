<?php 
	$page_id=1;
	include('includes/header.php'); 
?>
        <!-- content begin -->
        <div id="content" class="no-bottom no-top">

            <!-- revolution slider begin -->
            <section id="section-slider" class="fullwidthbanner-container" aria-label="section-slider">
                <div id="revolution-slider">
                    <ul>
                        <li data-transition="parallaxtoright" data-slotamount="10" data-masterspeed="800" data-thumb="">
                            <!--  BACKGROUND IMAGE -->
                            <img src="images-slider/wide4.jpg" alt="" />
                            <div class="tp-caption big-white sfb"
                                data-x="center"
                                data-y="180"
                                data-speed="800"
                                data-start="400"
                                data-easing="easeInOutExpo"
                                data-endspeed="450">
                                <span class="id-color">Our Expertise For</span>
                            </div>

                            <div class="tp-caption ultra-big-white sfb"
                                data-x="center"
                                data-y="center"
                                data-speed="800"
                                data-start="500"
                                data-easing="easeInOutExpo"
                                data-endspeed="400">
                                Interior Design
                            </div>

                            <div class="tp-caption text-center sfb"
                                data-x="center"
                                data-y="290"
                                data-speed="800"
                                data-start="600"
                                data-easing="easeInOutExpo"
                                data-endspeed="450">
                                We are team based on Calicut. Our expertise on Interior Design.
                                <br>
                                often including the exterior, of a room or building.
                            </div>

<!--
                            <div class="tp-caption sfb"
                                data-x="center"
                                data-y="375"
                                data-speed="800"
                                data-start="700"
                                data-easing="easeInOutExpo">
                                <a href="#" class="btn-slider">Our Portfolio
                                </a>
                            </div>
-->
                        </li>

                        <li data-transition="parallaxtoright" data-slotamount="10" data-masterspeed="800" data-thumb="">
                            <!--  BACKGROUND IMAGE -->
                            <img src="images-slider/wide5.jpg" alt="" />
                            <div class="tp-caption big-white sfb"
                                data-x="center"
                                data-y="180"
                                data-speed="800"
                                data-start="400"
                                data-easing="easeInOutExpo"
                                data-endspeed="450">
                                <span class="id-color">Featured Project</span>
                            </div>

                            <div class="tp-caption ultra-big-white sfb"
                                data-x="center"
                                data-y="center"
                                data-speed="800"
                                data-start="500"
                                data-easing="easeInOutExpo"
                                data-endspeed="400">
                                Elegant Interior
                            </div>

                            <div class="tp-caption text-center sfb"
                                data-x="center"
                                data-y="290"
                                data-speed="800"
                                data-start="600"
                                data-easing="easeInOutExpo"
                                data-endspeed="450">
                                We are team based on Calicut. Our expertise on Interior Design.
                                <br>
                                often including the exterior, of a room or building.
                            </div>

<!--
                            <div class="tp-caption sfb"
                                data-x="center"
                                data-y="375"
                                data-speed="800"
                                data-start="700"
                                data-easing="easeInOutExpo">
                                <a href="#" class="btn-slider">Our Portfolio
                                </a>
                            </div>
-->
                        </li>

                        

                    </ul>
                </div>
            </section>
            <!-- revolution slider close -->

            <section id="section-why-choose-us-2" class="about_wrap">
                <div class="container">
                    <h1>Bodhi Interiors</h1>
                    <p>An interior design firm driven from highly imaginative design solutions that satisfy human, physical, technical, aesthetic and functional needs. We’ll work with your tastes, preferences, and space to create your ideal environment.
We provides professional architecture and design services from initial concept planning and budgeting to executing all phases of construction.
Bodhi Concepteurs oversees each and every detail so you don’t have to saving you time, worry and money.  The team assures that the design you have worked on is executed with reliable and reputable trade sources professionally on time and on budget.
</p>
                </div>
            </section>

            <section id="section-why-choose-us-2">

                <div class="container">
                    <div class="row">

                        <div class="col-md-4 wow fadeInUp" data-wow-delay="0s">
                            <div class="box-icon">
                                <span class="icon wow fadeIn" data-wow-delay=".5s"><i class="id-color icon-paintbrush"></i></span>
                                <div class="text">
                                    <h3>VR Experience</h3>
                                    <p>3D walk through and virtual reality technology lets you to experience your furnished home before you buy.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 wow fadeInUp" data-wow-delay=".25s">
                            <div class="box-icon">
                                <span class="icon wow fadeIn" data-wow-delay=".75s"><i class="id-color icon-trophy"></i></span>
                                <div class="text">
                                    <h3>Irreplaceable implementation</h3>
                                    <p>You will get a personal project manager. They will give you the daily updates from site.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 wow fadeInUp" data-wow-delay=".75s">
                            <div class="box-icon">
                                <span class="icon wow fadeIn" data-wow-delay="1.25s"><i class="id-color icon-chat"></i></span>
                                <div class="text">
                                    <h3>Built to last</h3>
                                    <p>We ensure the best raw materials</p>
                                </div>
                            </div>
                        </div>


                        <div class="spacer-single"></div>

                        <!--<div class="col-md-4 wow fadeInUp" data-wow-delay=".5s">
                            <div class="box-icon">
                                <span class="icon wow fadeIn" data-wow-delay="1s"><i class="id-color icon-wallet"></i></span>
                                <div class="text">
                                    <h3>Reasonable Price</h3>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 wow fadeInUp" data-wow-delay="1s">
                            <div class="box-icon">
                                <span class="icon wow fadeIn" data-wow-delay="1.5s"><i class="id-color icon-recycle"></i></span>
                                <div class="text">
                                    <h3>Guaranteed Works</h3>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 wow fadeInUp" data-wow-delay="1.25s">
                            <div class="box-icon">
                                <span class="icon wow fadeIn" data-wow-delay="1.75s"><i class="id-color icon-envelope"></i></span>
                                <div class="text">
                                    <h3>24 / 7 Support</h3>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae.</p>
                                </div>
                            </div>
                        </div>-->
                    </div>
                </div>
            </section>

     <!--        <section id="section-why-choose-us-2" class="offer_wrap">
                <div class="container">
                    <a href="offers.php"><img src="../images/homePageBanner.png" alt="Bodhi"></a>
                </div>
            </section>
     -->       
            <!-- section begin -->
            <section id="section-about" class="no-top no-bottom">
                <div class="container-fluid">
                    <div class="row-fluid display-table">

                        <div class="col-md-4 text-middle text-light wow fadeInRight" data-wow-delay="0" data-bgimage="url(images/services/p2_a.jpg)">
                            <div class="padding40 overlay70">
                                <h3>Residential Design</h3>
                                <p>Our commitment to quality and services ensure our clients happy. With years of experiences and continuing research, our team is ready to serve your interior design needs. We're happy to make you feel more comfortable in your home.</p>
                                <!--<a href="#" class="btn-line btn-fullwidth">Read More</a>-->
                            </div>
                        </div>

                        <div class="col-md-4 text-middle text-light wow fadeInRight" data-wow-delay=".1s" data-bgimage="url(images/services/p3_b.jpg)">
                            <div class="padding40 overlay70">
                                <h3>Office Design</h3>
                                <p>This is true because the office building is the most tangible reflection of a profound change in employment patterns. We design in such a way that the surroundings, the colour  will help to improve the productivity of the workplace.</p>
                                <!--<a href="#" class="btn-line btn-fullwidth">Read More</a>-->
                            </div>
                        </div>
                        <div class="col-md-4 text-middle text-light wow fadeInRight" data-wow-delay=".3s" data-bgimage="url(images/services/p4_a.jpg)">
                            <div class="padding40 overlay70">
                                <h3>Commercial Design</h3>
                                <p>Our commitment to quality and services ensure our clients happy. With years of experiences and continuing research, our team is ready to serve your interior design needs. We're happy to make you feel more comfortable in your home.</p>
                                <!--<a href="#" class="btn-line btn-fullwidth">Read More</a>-->

                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <!-- section close -->

            <!-- section begin -->
            <section id="section-portfolio-2" class="no-top no-bottom" data-bgcolor="#252525" aria-label="section-portfolio">
                <div class="container">

                    <div class="spacer-single"></div>

                    <!-- portfolio filter begin -->
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <ul id="filters" class="wow fadeInUp" data-wow-delay="0s">
                                <li><a href="#" data-filter="*" class="selected">All Projects</a></li>
                                <li><a href="#" data-filter=".residential">Residential</a></li>
                                <li><a href="#" data-filter=".hospitaly">Hospitaly</a></li>
                                <li><a href="#" data-filter=".office">Office</a></li>
                                <li><a href="#" data-filter=".commercial">Commercial</a></li>
                            </ul>

                        </div>
                    </div>
                    <!-- portfolio filter close -->

                </div>

                <div id="gallery" class="gallery full-gallery de-gallery pf_full_width wow fadeInUp" data-wow-delay=".3s">

                    <!-- gallery item -->
                    <div class="item residential">
                        <div class="picframe">
                            <a class="simple-ajax-popup-align-top" href="project-details-1.html">
                                <span class="overlay">
                                    <span class="pf_text">
                                        <span class="project-name">Eco Green Interior</span>
                                    </span>
                                </span>
                            </a>
                            <img src="images/portfolio/pf%20(1).jpg" alt="" />
                        </div>
                    </div>
                    <!-- close gallery item -->

                    <!-- gallery item -->
                    <div class="item hospitaly">
                        <div class="picframe">
                            <a class="simple-ajax-popup-align-top" href="project-details-2.html">
                                <span class="overlay">
                                    <span class="pf_text">
                                        <span class="project-name">Modern Elegance Suite</span>
                                    </span>
                                </span>
                            </a>

                            <img src="images/portfolio/pf%20(2).jpg" alt="" />
                        </div>
                    </div>
                    <!-- close gallery item -->

                    <!-- gallery item -->
                    <div class="item hospitaly">
                        <div class="picframe">
                            <a class="simple-ajax-popup-align-top" href="project-details-3.html">
                                <span class="overlay">
                                    <span class="pf_text">
                                        <span class="project-name">Apartment Renovation</span>
                                    </span>
                                </span>
                            </a>

                            <img src="images/portfolio/pf%20(3).jpg" alt="" />
                        </div>
                    </div>
                    <!-- close gallery item -->

                    <!-- gallery item -->
                    <div class="item residential">
                        <div class="picframe">
                            <a class="simple-ajax-popup-align-top" href="project-details-youtube.html">
                                <span class="overlay">
                                    <span class="pf_text">
                                        <span class="project-name">Youtube Video</span>
                                    </span>
                                </span>
                            </a>
                            <img src="images/portfolio/pf%20(4).jpg" alt="" />
                        </div>
                    </div>
                    <!-- close gallery item -->

                    <!-- gallery item -->
                    <div class="item office">
                        <div class="picframe">
                            <a class="simple-ajax-popup-align-top" href="project-details-vimeo.html">
                                <span class="overlay">
                                    <span class="pf_text">
                                        <span class="project-name">Vimeo Video</span>
                                    </span>
                                </span>
                            </a>
                            <img src="images/portfolio/pf%20(5).jpg" alt="" />
                        </div>
                    </div>
                    <!-- close gallery item -->

                    <!-- gallery item -->
                    <div class="item commercial">
                        <div class="picframe">
                            <a class="simple-ajax-popup-align-top" href="project-details.html">
                                <span class="overlay">
                                    <span class="pf_text">
                                        <span class="project-name">Restaurant In Texas</span>
                                    </span>
                                </span>
                            </a>
                            <img src="images/portfolio/pf%20(6).jpg" alt="" />
                        </div>
                    </div>
                    <!-- close gallery item -->

                    <!-- gallery item -->
                    <div class="item residential">
                        <div class="picframe">
                            <a class="simple-ajax-popup-align-top" href="project-details-youtube.html">
                                <span class="overlay">
                                    <span class="pf_text">
                                        <span class="project-name">Summer House</span>
                                    </span>
                                </span>
                            </a>

                            <img src="images/portfolio/pf%20(7).jpg" alt="" />
                        </div>
                    </div>
                    <!-- close gallery item -->

                    <!-- gallery item -->
                    <div class="item office">
                        <div class="picframe">
                            <a class="simple-ajax-popup-align-top" href="project-details-vimeo.html">
                                <span class="overlay">
                                    <span class="pf_text">
                                        <span class="project-name">Office On Space</span>
                                    </span>
                                </span>
                            </a>

                            <img src="images/portfolio/pf%20(8).jpg" alt="" />
                        </div>
                    </div>
                    <!-- close gallery item -->

                </div>

                <div id="loader-area">
                    <div class="project-load"></div>
                </div>
            </section>
            <!-- section close -->

            <!-- section begin -->
            <section id="section-steps-3">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 text-center wow fadeInUp">
                            <h1>Our Process</h1>
                            <div class="separator"><span><i class="fa fa-circle"></i></span></div>
                            <div class="spacer-single"></div>
                        </div>

                        <div class="col-md-12">
                            <div class="de_tab tab_steps">
                                <ul class="de_nav">
                                    <li class="active wow fadeIn" data-wow-delay="0s"><span>Meet &amp; Agree</span><div class="v-border"></div>
                                    </li>
                                    <li class="wow fadeIn" data-wow-delay=".2s"><span>Idea &amp; Concept</span><div class="v-border"></div>
                                    </li>
                                    <li class="wow fadeIn" data-wow-delay=".6s"><span>Design &amp; Create</span><div class="v-border"></div>
                                    </li>
                                    <li class="wow fadeIn" data-wow-delay=".8s"><span>Build &amp; Install</span><div class="v-border"></div>
                                    </li>
                                </ul>

                               <!-- <div class="de_tab_content">

                                    <div id="tab1">
                                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam.
                                    </div>

                                    <div id="tab2">
                                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam.
                                    </div>

                                    <div id="tab3">
                                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam.
                                    </div>

                                    <div id="tab4">
                                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam.
                                    </div>

                                </div>-->
                            <style>.de_tab.tab_steps .de_nav li .v-border{display:none !important;}</style>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- section close -->

            <!-- content begin -->

            <!--<section id="section-fun-facts" class="side-bg no-padding text-light">
                <div class="image-container col-md-5 pull-left" data-delay="0"></div>

                <div class="container">
                    <div class="row">
                        <div class="inner-padding">
                            <div class="col-md-6 col-md-offset-6">
                                <div class="row padding20">

                                    <div class="col-md-6 wow fadeIn" data-wow-delay="0">
                                        <div class="de_count">
                                            <h3 class="timer" data-to="2350" data-speed="2500">0</h3>
                                            <span>Hours of Works</span>
                                        </div>
                                    </div>

                                    <div class="col-md-6 wow fadeIn" data-wow-delay=".25s">
                                        <div class="de_count">
                                            <h3 class="timer" data-to="128" data-speed="2500">0</h3>
                                            <span>Projects Complete</span>
                                        </div>
                                    </div>

                                    <div class="col-md-6 wow fadeIn" data-wow-delay=".5s">
                                        <div class="de_count">
                                            <h3 class="timer" data-to="750" data-speed="2500">0</h3>
                                            <span>Slice of Pizza</span>
                                        </div>
                                    </div>

                                    <div class="col-md-6 wow fadeIn" data-wow-delay=".75s">
                                        <div class="de_count">
                                            <h3 class="timer" data-to="520" data-speed="2500">0</h3>
                                            <span>Cups of Coffee</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </section>-->

            <!-- section begin -->
            <section id="view-all-projects" class="call-to-action bg-color text-center padding40" data-speed="5" data-type="background" aria-label="view-all-projects">
                <a href="contact.php" class="btn btn-line-black btn-big wow fadeInUp">Get Quotation</a>
            </section>
            <!-- logo carousel section close -->

        </div>
<?php include('includes/footer.php');?>
<?php 
	$page_id=4;
	include('includes/header.php'); 
?>

        <!-- subheader -->
        <section id="subheader" data-speed="8" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>About Us</h1>
                        <ul class="crumb">
                            <li><a href="index.php">Home</a></li>
                            <li class="sep">/</li>
                            <li>About Us</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- subheader close -->

        <!-- content begin -->
        <div id="content" class="no-top no-bottom">
            <section id="section-about-us-2" class="side-bg no-padding">
                <div class="image-container col-md-5 pull-left" data-delay="0"></div>

                <div class="container">
                    <div class="row">
                        <div class="inner-padding">
                            <div class="col-md-6 col-md-offset-6 " data-animation="fadeInRight" data-delay="200">
                                <h2>Bodhi Interior Design</h2>

                                <p class="intro">An interior design firm driven from highly imaginative design solutions that satisfy human, physical, technical, aesthetic and functional needs. We’ll work with your tastes, preferences, and space to create your ideal environment.</p>

                                
We provides professional architecture and design services from initial concept planning and budgeting to executing all phases of construction. Bodhi Concepteurs oversees each and every detail so you don’t have to saving you time, worry and money.  The team assures that the design you have worked on is executed with reliable and reputable trade sources professionally on time and on budget.
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </section>

<!--
            <section id="section-team">
                <div class="container">



                    <div class="row">
                        <div class="col-md-12 container-4">
                            
                            <div class="de-team-list">
                                <div class="team-pic">
                                    <img src="images/team/team_pic_1.jpg" class="img-responsive" alt="" />
                                </div>
                                <div class="team-desc col-md-12">
                                    <h3>John Smith</h3>
                                    <p class="lead">Project Manager</p>
                                    <div class="small-border"></div>
                                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                    <div class="social">
                                        <a href="#"><i class="fa fa-facebook fa-lg"></i></a>
                                        <a href="#"><i class="fa fa-twitter fa-lg"></i></a>
                                        <a href="#"><i class="fa fa-google-plus fa-lg"></i></a>
                                        <a href="#"><i class="fa fa-skype fa-lg"></i></a>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="de-team-list">
                                <div class="team-pic">
                                    <img src="images/team/team_pic_2.jpg" class="img-responsive" alt="" />
                                </div>
                                <div class="team-desc col-md-12">
                                    <h3>Michael Dennis</h3>
                                    <p class="lead">Creative Director</p>
                                    <div class="small-border"></div>
                                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                    <div class="social">
                                        <a href="#"><i class="fa fa-facebook fa-lg"></i></a>
                                        <a href="#"><i class="fa fa-twitter fa-lg"></i></a>
                                        <a href="#"><i class="fa fa-google-plus fa-lg"></i></a>
                                        <a href="#"><i class="fa fa-skype fa-lg"></i></a>
                                    </div>
                                </div>
                            </div>
                            

                            
                            <div class="de-team-list">
                                <div class="team-pic">
                                    <img src="images/team/team_pic_3.jpg" class="img-responsive" alt="" />
                                </div>
                                <div class="team-desc col-md-12">
                                    <h3>Sarah Michelle</h3>
                                    <p class="lead">Creative Staff</p>
                                    <div class="small-border"></div>
                                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                    <div class="social">
                                        <a href="#"><i class="fa fa-facebook fa-lg"></i></a>
                                        <a href="#"><i class="fa fa-twitter fa-lg"></i></a>
                                        <a href="#"><i class="fa fa-google-plus fa-lg"></i></a>
                                        <a href="#"><i class="fa fa-skype fa-lg"></i></a>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class="de-team-list">
                                <div class="team-pic">
                                    <img src="images/team/team_pic_4.jpg" class="img-responsive" alt="" />
                                </div>
                                <div class="team-desc col-md-12">
                                    <h3>Katty Wilson</h3>
                                    <p class="lead">Creative Staff</p>
                                    <div class="small-border"></div>
                                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                    <div class="social">
                                        <a href="#"><i class="fa fa-facebook fa-lg"></i></a>
                                        <a href="#"><i class="fa fa-twitter fa-lg"></i></a>
                                        <a href="#"><i class="fa fa-google-plus fa-lg"></i></a>
                                        <a href="#"><i class="fa fa-skype fa-lg"></i></a>
                                    </div>
                                </div>
                            </div>
                            

                            <div class="clearfix"></div>

                        </div>
                    </div>
                </div>
            </section>
-->

<!--
            <section id="section-testimonial" class="text-light">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 text-center wow fadeInUp">
                            <h1>Customer Says</h1>
                            <div class="separator"><span><i class="fa fa-circle"></i></span></div>
                            <div class="spacer-single"></div>
                        </div>
                    </div>
                    <div id="testimonial-carousel" class="de_carousel  wow fadeInUp" data-wow-delay=".3s">

                        <div class="col-md-6 item">
                            <div class="de_testi">
                                <blockquote>
                                    <p>I'm always impressed with the services. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                                    <div class="de_testi_by">
                                        John, Customer
                                    </div>
                                </blockquote>

                            </div>
                        </div>

                        <div class="col-md-6 item">
                            <div class="de_testi">
                                <blockquote>
                                    <p>I have very much enjoyed with your services. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                                    <div class="de_testi_by">
                                        Michael, Customer
                                    </div>
                                </blockquote>
                            </div>
                        </div>

                        <div class="col-md-6 item">
                            <div class="de_testi">
                                <blockquote>
                                    <p>I totally recommend your services. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                                    <div class="de_testi_by">
                                        Patrick, Customer
                                    </div>
                                </blockquote>
                            </div>
                        </div>

                        <div class="col-md-6 item">
                            <div class="de_testi">
                                <blockquote>
                                    <p>I have very much enjoyed with your services. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                                    <div class="de_testi_by">
                                        James, Customer
                                    </div>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
-->

            <!-- section begin -->
            <section id="view-all-projects" class="call-to-action bg-color dark text-center" data-speed="5" data-type="background" aria-label="view-all-projects">
                <a href="contact.php" class="btn btn-line-black btn-big">Talk With Us</a>
            </section>
            <!-- logo carousel section close -->



        </div>


<?php include('includes/footer.php');?>
<?php 
	$page_id=5;
	include('includes/header.php'); 
?>
        <!-- subheader -->
        <section id="subheader" data-speed="8" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Contact</h1>
                        <ul class="crumb">
                            <li><a href="index.php">Home</a></li>
                            <li class="sep">/</li>
                            <li>Contact</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- subheader close -->

        <!-- content begin -->
        <div id="content">
            <!--<section class="no-top" aria-label="map-container">
                <div id="map"></div>
            </section>-->

            <div class="container">
                <div class="row">

                    <div class="col-md-8">
                        <h3>Send Us Message</h3>
                        <form name="contactForm" id='contact_form' method="post" action='http://www.themenesia.com/themeforest/archi/email.php'>
                            <div class="row">
                                <div class="col-md-4">
                                    <div id='name_error' class='error'>Please enter your name.</div>
                                    <div>
                                        <input type='text' name='name' id='name' class="form-control" placeholder="Your Name">
                                    </div>

                                    <div id='email_error' class='error'>Please enter your valid E-mail ID.</div>
                                    <div>
                                        <input type='text' name='email' id='email' class="form-control" placeholder="Your Email">
                                    </div>

                                    <div id='phone_error' class='error'>Please enter your phone number.</div>
                                    <div>
                                        <input type='text' name='phone' id='phone' class="form-control" placeholder="Your Phone">
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div id='message_error' class='error'>Please enter your message.</div>
                                    <div>
                                        <textarea name='message' id='message' class="form-control" placeholder="Your Message"></textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <p id='submit'>
                                        <input type='submit' id='send_message' value='Submit Form' class="btn btn-line">
                                    </p>
                                    <div id='mail_success' class='success'>Your message has been sent successfully.</div>
                                    <div id='mail_fail' class='error'>Sorry, error occured this time sending your message.</div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div id="sidebar" class="col-md-4">

                        <div class="widget widget_text">
                            <h3>Contact Info</h3>
                            <address>
                                <span>3rd Floor, CD Tower
Opp. Baby Memorial Hospital
Calicut</span>
                                <span><strong>Phone:</strong>+91 7560940007</span>
                                <span><strong>Fax:</strong>+91 7560940007</span>
                                <span><strong>Email:</strong><a href="mailto:mail@designsbodhi.com">mail@designsbodhi.com</a></span>
                                <span><strong>Web:</strong><a href="#">www.designsbodhi.com</a></span>
                            </address>
                        </div>









                    </div>
                </div>
            </div>
        </div>



<?php include('includes/footer.php');?>

<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Bodhi Concepteurs is a construction company which  specializes in Architecture, Structural Engineering, 
	Property Management services, Landscape design, Home automation, Floor plan & elevation.">
	<meta name="Keywords" content="Architecture, Structural Engineering, Property Management services, Landscape design, Home automation, Floor plan planner, Elevation, Home design" />
    <meta name="author" content="">
	
	<meta property="og:type" content="Website" />
    <meta property="og:url" content="http://designsbodhi.com/" />
    <meta property="og:title" content="Bodhi Concepteurs : Building contractors" />
    <meta property="og:description" content="Bodhi Concepteurs is a construction company which  specializes in Architecture, Structural Engineering, 
	Property Management services, Landscape design, Home automation, Floor plan & elevation." />
    <meta property="og:image" content="http://designsbodhi.com/images/og-img.jpg" />
	
    <title>Bodhi Concepteurs | Building contractors</title>
    <link rel="shortcut icon" href="">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/propeller.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/ligtht.css">
    <link rel="stylesheet" href="css/ma5slider.min.css">
    <link rel="stylesheet" href="css/jquery.bxslider.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/lightgallery.css">
    <link rel="stylesheet" href="css/stylec894.css">
    <link rel="stylesheet" href="css/style933c.css">
    <link rel="stylesheet" href="css/et-line-font933c.css">
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/twentytwenty-no-compass.css">
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.css'>
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.css'>
    <link rel="stylesheet" href="css/style.css"> </head>



<body>
    
    <?php if($page_id==1){ ?>
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loading_page">
            <div class="container load_ing">
                <article id="wrap">
                    <div class="logo"> <img src="images/logo.png" alt="Bodhi Concepteurs">
                        <div class="border_loading">
                            <div class="ex ex--light"><div class="mnml-spinner dark mx-auto"></div></div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
<?php } ?>
    
<?php if($page_id==1){ ?>
    <header>
        <div class="header_bg_transition">
            <ul class="bg_bxslider">
              <li style="background: url('images/57c8b543253643.57e957e02ae33-1024x614.jpg');"></li>
              <li style="background: url('images/after.jpg');"></li>
              <li style="background: url('images/57c8b543253643.57e957e02ae33-1024x614.jpg');"></li>
              <li style="background: url('images/after.jpg');"></li>
            </ul>
        </div>
        <div class="container">
            <div class="logo_wrap">
                        <div class="logo"> <img src="images/logo.png" alt="Bodhi Concepteurs"> </div>
                        <div class="social_media"><a href="#"><span><i class="fa fa-instagram" aria-hidden="true"></i></span></a><a href="#"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a> <a href="#"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a> <a href="#"><span><i class="fa fa-linkedin" aria-hidden="true"></i></span></a></div> </div>
            <div class="estimate offers esti_align"> <a class="pmd-ripple-effect" href="#" data-target="#simple-dialog" data-toggle="modal">Get an Estimate</a> </div>
            <div class="estimate offers"> <a class="pmd-ripple-effect" href="intirio/offers.php" >OFFERS</a> </div>
            <!-- Simple alert -->
            <div tabindex="-1" class="modal fade" id="simple-dialog" style="display: none;" aria-hidden="true">
                <div class="modal-dialog">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="about_company get_estimate">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="contant">
                                            <h1>Get an Estimate</h1>
<!--                                            <p>PLEASE PROVIDE SOME DETAILS TO GET AN ESTIMATE.</p>-->
                                        </div>
                                        <form>
                                            <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                                <label for="regular1" class="control-label"> Your Name* </label>
                                                <input type="text" id="regular1" class="form-control" required> </div>
                                            <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                                <label for="regular1" class="control-label"> Contact Number*</label>
                                                <input type="tel" id="regular1" class="form-control" required onkeypress='return event.charCode >= 48 && event.charCode <= 57'> </div>
                                            <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                                <label for="regular1" class="control-label"> Email*</label>
                                                <input type="email" id="regular1" class="form-control" required> </div>
                                            <!--Simple select -->
                                            <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                                <label>Projects</label>
                                                <select class="select-simple form-control pmd-select2" required>
                                                    <option></option>
                                                    <option>Flat</option>
                                                    <option>Villa</option>
                                                    <option>Office</option>
                                                </select>
                                            </div>
                                            <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                                <label for="regular1" class="control-label"> Project Location*</label>
                                                <input type="text" id="regular1" class="form-control" required> </div>
                                            <button type="submit" class="btn btn-lg pmd-btn-raised pmd-ripple-effect btn-primary">Send</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php include('includes/menu.php');?>
        </div>
    </header>
  <?php } ?>
    

    <?php if($page_id==2 || $page_id==3 || $page_id==4 || $page_id==5 || $page_id==6 || $page_id==7 || $page_id==8 || $page_id==9 || $page_id==10 || $page_id==11 || $page_id==12){ ?>
            
    <header class="inner_pages">
        <div class="header_bg_transition">
            <ul class="bg_bxslider">
              <li style="background: url('images/57c8b543253643.57e957e02ae33-1024x614.jpg');"></li>
              <li style="background: url('images/after.jpg');"></li>
              <li style="background: url('images/57c8b543253643.57e957e02ae33-1024x614.jpg');"></li>
              <li style="background: url('images/after.jpg');"></li>
            </ul>
        </div>
        <div class="container">
            <article id="wrap">
                <div class="logo"> <img src="images/logo.png" alt="Bodhi Concepteurs"> </div>
            </article>
            <div class="estimate"> <a class="btn pmd-ripple-effect btn-success" href="#" data-target="#simple-dialog" data-toggle="modal">Get an Estimate</a> </div>
            <!-- Simple alert -->
            <div tabindex="-1" class="modal fade" id="simple-dialog" style="display: none;" aria-hidden="true">
                <div class="modal-dialog">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="about_company get_estimate">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="contant">
                                            <h1>Get an Estimate</h1><!--
                                            <p>PLEASE PROVIDE SOME DETAILS TO GET AN ESTIMATE.</p>-->
                                        </div>
                                        <form>
                                            <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                                <label for="regular1" class="control-label"> Your Name* </label>
                                                <input type="text" id="regular1" class="form-control" required> </div>
                                            <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                                <label for="regular1" class="control-label"> Contact Number*</label>
                                                <input type="tel" id="regular1" class="form-control" required onkeypress='return event.charCode >= 48 && event.charCode <= 57'> </div>
                                            <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                                <label for="regular1" class="control-label"> Email*</label>
                                                <input type="email" id="regular1" class="form-control" required> </div>
                                            <!--Simple select -->
                                            <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                                <label>Projects</label>
                                                <select class="select-simple form-control pmd-select2" required>
                                                    <option></option>
                                                    <option>Flat</option>
                                                    <option>Villa</option>
                                                    <option>Office</option>
                                                </select>
                                            </div>
                                            <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                                <label for="regular1" class="control-label"> Project Location*</label>
                                                <input type="text" id="regular1" class="form-control" required> </div>
                                            <button type="submit" class="btn btn-lg pmd-btn-raised pmd-ripple-effect btn-primary">Send</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include('includes/menu.php');?>
    </header>
    <?php } ?>
        <a href="#" id="back-to-top" title="Back to top">&uarr;</a>
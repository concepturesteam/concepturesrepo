    <footer>
            <?php if($page_id==1){ ?>
<hr class="green">
<div class="sisters">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="bodhi_info">
                    <h3>OUR SISTER CONCERN</h3>
                    <img src="images/bodhi.png" alt="Bodhi Info Solutions Pvt Ltd">
                    <h1>BODHI INFO SOLUTIONS PVT LTD</h1>
                    <p>Bodhi is a culmination of creative young minds &amp; experienced professionals, who strive hard to deliver their best. We follow a client-centric approach, where in we believe that a measure of our success is only as much as your success.</p>
                    <a href="http://bodhiinfo.com/" target="_blank">VISIT WEBSITE</a>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="bodhi_info lapic">
                    <img src="images/lapic.png" alt="Lapic">
                    <h1>We are Expertise in
                        <b>Structural Designing</b></h1>
                    <a href="#">VISIT WEBSITE</a>
                </div>
            </div>
        </div>
    </div>
</div>
        <?php } ?>
        <?php if($page_id==2 || $page_id==3 || $page_id==4 || $page_id==5 || $page_id==6 || $page_id==7 || $page_id==8 || $page_id==9 || $page_id==10 || $page_id==11 || $page_id==12){ ?>
        <div class="social_media"> <a href="#"><span><i class="fa fa-instagram" aria-hidden="true"></i></span></a> <a href="#"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a> <a href="https://www.facebook.com/BodhiConcepteurs/"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a> <a href="#"><span><i class="fa fa-linkedin" aria-hidden="true"></i></span></a> </div><?php } ?>
        <div class="copy">
            <div class="container">
                <p>2017 Copyright © Bodhi Concepteurs All Rights Reserved</p>
                <!--<span>Powered By &nbsp; <a href="http://www.bodhiinfo.com" target="_blank"><img src="images/bodhi.png"></a></span>-->
            </div>
        </div>
    </footer>
</div>
<script>    //  analytics code   designbodhigmail
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-99257243-1', 'auto');
  ga('send', 'pageview');

</script>

    <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <!--<script src="js/jquery-3.1.1.min.js"></script>-->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/propeller.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/ma5slider.min.js"></script>
    <script src="js/functions.js"></script>
    <script src="js/run_prettify.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/modernizr-2.6.2.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/lightgallery.min.js"></script>
    <script src="js/jquery.event.move.js"></script>
    <script src="js/jquery.twentytwenty.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js'></script>
    <script src="js/scripts.js"></script>
    <script>
        $( function() {
          var $winHeight = $( window ).height()
          $( '.load_ing' ).height( $winHeight );
        });
        jakealbaughSignature("light");
        
       
    </script>
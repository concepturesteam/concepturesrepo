<?php 
	$page_id=4;
	include('includes/header.php'); 
?>

<div class="inner_layout">
    <div class="about_company">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="contant">
                        <h1>Kerala SIDCO </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="projects_inner">
        <div class="container">
            <div class="project_slid">
                <ul class="bxslider">
                    <li style="background:url('images/pexels-photo.jpg') center no-repeat; background-size:cover;">
                        <div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://www.youtube.com/embed/SjNo63SSo9o?ecver=2" width="640" height="500" frameborder="0" style="position:absolute;width:100%;left:0" allowfullscreen></iframe></div>
                    </li>
                    <li style="background:url('images/projects/1.jpg') center no-repeat; background-size:cover;">
                    </li>
                    <li style="background:url('images/projects/2.jpg') center no-repeat; background-size:cover;">
                    </li>
                    <li style="background:url('images/projects/4.jpg') center no-repeat; background-size:cover;">
                    </li>
                    <li style="background:url('images/projects/6.jpg') center no-repeat; background-size:cover;">
                    </li>
                    <li style="background:url('images/projects/8.jpg') center no-repeat; background-size:cover;">
                    </li>
                    <li style="background:url('images/projects/8_1.jpg') center no-repeat; background-size:cover;">
                    </li>
                </ul>
            </div>
        </div>
    </div>
        <div class="container">
            <div class="next_paginations">
                <a href="#">PREV PAGE</a>
                <a href="#">NEXT PAGE</a>
            </div>
        </div>
</div>
    <?php include('includes/footer.php');?>
        <script>
            $(document).ready(function(){
                $('.carousel[data-type="multi"] .item').each(function(){
                  var next = $(this).next();
                  if (!next.length) {
                    next = $(this).siblings(':first');
                  }
                  next.children(':first-child').clone().appendTo($(this));

                  for (var i=0;i<4;i++) {
                    next=next.next();
                    if (!next.length) {
                        next = $(this).siblings(':first');
                    }

                    next.children(':first-child').clone().appendTo($(this));
                  }
                });
            });
            $(window).on('load', function () {
                $('.ma5slider').ma5slider({
                    autoplayTime: 8e5

                });
            });
            $(document).ready(function () {
                $('.bxslider').bxSlider({
                    pager: false,
                    auto: true,
                    autoControls: true
                });
                $('.bxslider_carousel').bxSlider({
                      auto: true,
                      pager: false,
                    minSlides: 1,
                    maxSlides: 6,
                    moveSlides: 1,
                      slideWidth: 150,
                      slideMargin: 10
                });
            });
        </script>
        </body>

        </html>
<?php 
	$page_id=5;
	include('includes/header.php'); 
?>
<div class="inner_layout">
   <!-- <div class="projects">
        <div class="project_slid">
            <ul class="bxslider">
                <li style="background:url('images/homePageBanner.png') center no-repeat; background-size:cover;">
                </li>
                <li style="background:url('images/pexels-photo_1.jpg') center no-repeat; background-size:cover;">
                </li>
                <li style="background:url('images/pexels-photo.jpg') center no-repeat; background-size:cover;">
                </li>
            </ul>
        </div>
    </div>-->
    <div class="about_company clientele_wrap">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="contant">
                        <h1>Clientele</h1>
                        <!--<h1 class="bodhi_color">BODHI CONCEPTEURS</h1>-->
                        <p> Bodhi Concepteurs consists of a dynamic, talented and experienced team of professionals experts at customizing architectural designs that stand out in the crowd leaving no barriers of half-baked premises.</p> </div>
                </div>
            </div>
        </div>
    </div>
    <div class="client_list">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="image_list">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <img src="images/logo-b.jpg" alt="Bodhi Concepteurs" />
                                <h4>Kozhikoden's</h4>
                                <img src="images/logo-d.jpg" alt="Bodhi Concepteurs" />
                                <h4>SIDCO</h4>
                                <img src="images/logo-c.png" alt="Bodhi Concepteurs" />
                                <h4>Royal Paris</h4>
                            </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9">
                    <div class="name_list">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <h4>Bodhi Concepteurs</h4>
                                <p>Bodhi Concepteurs</p>
                                <h4>Bodhi Concepteurs</h4>
                                <p>Bodhi Concepteurs</p>
                                <h4>Bodhi Concepteurs</h4>
                                <p>Bodhi Concepteurs</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


    <?php include('includes/footer.php');?>
        <script>
            $(document).ready(function(){
                $('.carousel[data-type="multi"] .item').each(function(){
                  var next = $(this).next();
                  if (!next.length) {
                    next = $(this).siblings(':first');
                  }
                  next.children(':first-child').clone().appendTo($(this));

                  for (var i=0;i<4;i++) {
                    next=next.next();
                    if (!next.length) {
                        next = $(this).siblings(':first');
                    }

                    next.children(':first-child').clone().appendTo($(this));
                  }
                });
            });
            $(window).on('load', function () {
                $('.ma5slider').ma5slider({
                    autoplayTime: 8e5

                });
            });
            $(document).ready(function () {
                $('.bxslider').bxSlider({
                    pager: false,
                    auto: true,
                    autoControls: true
                });
                $('.bxslider_carousel').bxSlider({
                      auto: true,
                      pager: false,
                    minSlides: 1,
                    maxSlides: 6,
                    moveSlides: 1,
                      slideWidth: 150,
                      slideMargin: 10
                });
            });
        </script>
        </body>

        </html>
<?php 
	$page_id=2;
	include('includes/header.php'); 
?>
<div class="inner_layout">
    <div class="about_company about_inner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="contant">
                        <h1>About Us</h1>
<!--                        <h1 class="bodhi_color">BODHI CONCEPTEURS</h1>-->
                        <p>Bodhi Concepteurs consists of a dynamic, talented and experienced team of professionals experts at customizing architectural designs that stand out in the crowd leaving no barriers of half-baked premises. With a badge of unprecedented legacy in serving the engineering and architecture industry for more than 5 years now, we have our base located in Calicut tending to a loyal base of conventional clientele all over the world now.<br><br>
We guarantee stunningly graceful epitome of pinnacle living with no compromise over luxury and quality, while implementing world class materials to offer our customers with an experience one step ahead of what they expect from us. Bodhi Concepteur is a brand name that has managed to establish its own level of identity in the realm of building construction and architecture through a dedicated host of services in Calicut.
Be the one among the fortunate few to grab an opportunity of living a real life in the most beautiful homes you have ever dreamt of.
</p> </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mision_vision">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content_box">
                        <h2>VISION</h2>
                        <p>Bodhi Concepteurs aims at designing homes that signifies the embodiment of luxurious living in the heart of the most beautiful and happening cities in Kerala and it is none other than our own “Calicut”. Our goal is to emerge as the number one in the industry with our dedicated quality of services focusing on the scope of improvement with each of our projects.</p>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="content_box dark">
                        <h2>MISSION</h2>
                        <p>Our mission lies in building relationships through trust and dedication. We desire to build entities that are customer specific and within the range of their budget. Bodhi Concepteurs looks forward to bring about a revolution to the existing trends of design and replace it with world class concepts that are popular in the international market.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


    <?php include('includes/footer.php');?>
        <script>
            $(document).ready(function(){
                $('.carousel[data-type="multi"] .item').each(function(){
                  var next = $(this).next();
                  if (!next.length) {
                    next = $(this).siblings(':first');
                  }
                  next.children(':first-child').clone().appendTo($(this));

                  for (var i=0;i<4;i++) {
                    next=next.next();
                    if (!next.length) {
                        next = $(this).siblings(':first');
                    }

                    next.children(':first-child').clone().appendTo($(this));
                  }
                });
            });
            $(window).on('load', function () {
                $('.ma5slider').ma5slider({
                    autoplayTime: 8e5

                });
            });
            $(document).ready(function () {
                $('.bxslider').bxSlider({
                    pager: false,
                    auto: true,
                    autoControls: true
                });
                $('.bxslider_carousel').bxSlider({
                      auto: true,
                      pager: false,
                    minSlides: 1,
                    maxSlides: 6,
                    moveSlides: 1,
                      slideWidth: 150,
                      slideMargin: 10
                });
            });
        </script>
        </body>

        </html>
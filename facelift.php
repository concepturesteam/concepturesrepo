<?php 
	$page_id=4;
	include('includes/header.php'); 
?>
<div class="inner_layout">
    <div class="about_company">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="content">
<!--                        <div class="subtitle">FACELIFT</div> -->
                        <h2 class="title">FACELIFT</h2> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="projects_section">
        <div class="container">
            <!--Scrollable tab example -->
            <div class="twentytwenty-container"> <img src="images/after_new.jpg" /> <img src="images/before_new.jpg" /> </div>
            <div class="twentytwenty-container"> <img src="images/after_new.jpg" /> <img src="images/before_new.jpg" /> </div>
            <div class="twentytwenty-container"> <img src="images/after_new.jpg" /> <img src="images/before_new.jpg" /> </div>
            <div class="twentytwenty-container"> <img src="images/after_new.jpg" /> <img src="images/before_new.jpg" /> </div>
        </div>
    </div>
</div>


    <?php include('includes/footer.php');?>
        <script>
            $(document).ready(function(){
                $('.carousel[data-type="multi"] .item').each(function(){
                  var next = $(this).next();
                  if (!next.length) {
                    next = $(this).siblings(':first');
                  }
                  next.children(':first-child').clone().appendTo($(this));

                  for (var i=0;i<4;i++) {
                    next=next.next();
                    if (!next.length) {
                        next = $(this).siblings(':first');
                    }

                    next.children(':first-child').clone().appendTo($(this));
                  }
                });
            });
            $(window).on('load', function () {
                $('.ma5slider').ma5slider({
                    autoplayTime: 8e5

                });
            });
            $(document).ready(function () {
                $('.bxslider').bxSlider({
                    pager: false,
                    auto: true,
                    autoControls: true
                });
                
            });
            $(window).load(function () {
                $(".twentytwenty-container[data-orientation!='vertical']").twentytwenty({
                    default_offset_pct: 0.7
                });
                $(".twentytwenty-container[data-orientation='vertical']").twentytwenty({
                    default_offset_pct: 0.3
                    , orientation: 'vertical'
                });
            });
        </script>
        </body>

        </html>
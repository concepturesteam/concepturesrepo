<?php 
	$page_id=7;
	include('includes/header.php'); 
?>
<div class="inner_layout">
    <div class="projects">
        <div class="project_slid">
            <ul class="bxslider">
                <li style="background:url('images/homePageBanner.png') center no-repeat; background-size:cover;">
                </li>
                <li style="background:url('images/pexels-photo_1.jpg') center no-repeat; background-size:cover;">
                </li>
                <li style="background:url('images/pexels-photo.jpg') center no-repeat; background-size:cover;">
                </li>
            </ul>
        </div>
    </div>
    <div class="about_company">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="contant">
                        <h1>Offers</h1>
                        <!--<h1 class="bodhi_color">BODHI CONCEPTEURS</h1>-->
                        <p> Bodhi Concepteurs consists of a dynamic, talented and experienced team of professionals experts at customizing architectural designs that stand out in the crowd leaving no barriers of half-baked premises.</p> </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <!--Accordion with colored icon example -->
<div class="panel-group pmd-accordion" id="accordion4" role="tablist" aria-multiselectable="true" > 
	<div class="panel panel-warning"> 
		<div class="panel-heading" role="tab" id="headingOne">
			<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion4" href="#collapseOne4" aria-expanded="true" aria-controls="collapseOne4"  data-expandable="false"><i class="material-icons pmd-sm pmd-accordion-icon-left">mood</i> Collapsible Group Item #1 </a> </h4>
		</div>
		<div id="collapseOne4" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
			<div class="panel-body"> The word "accordion" typically conjures a mental image of your favorite polka band. However that’s not what we are talking about when referring to accordion menu. Although polka music can offer a rip-snorting good time, the term is associated with something different in the realm of web design. User interface accordions might refer to menus, widgets, or content areas which expand like the musical instrument. These interfaces have grown a lot more popular in recent years with the expansion of JavaScript. </div>
		</div>
	</div>
	<div class="panel panel-danger"> 
		<div class="panel-heading" role="tab" id="headingTwo">
			<h4 class="panel-title"><a  data-toggle="collapse" data-parent="#accordion4" href="#collapseTwo4" aria-expanded="false" aria-controls="collapseTwo4"  data-expandable="false"><i class="material-icons pmd-sm pmd-accordion-icon-left">account_balance</i> Collapsible Group Item #2 </a> </h4>
		</div>
		<div id="collapseTwo4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			<div class="panel-body">Accordions are popular because they allow developers to force large amounts of content into tiny spaces on the page. Granted these content displays also require dynamic effects for switching between page elements – so there are pros and cons to accordions.  </div>
		</div>
	</div>
	<div class="panel panel-success"> 
		<div class="panel-heading" role="tab" id="headingThree">
			<h4 class="panel-title"> <a  data-toggle="collapse" data-parent="#accordion4" href="#collapseThree4" aria-expanded="false" aria-controls="collapseThree4"  data-expandable="false"><i class="material-icons pmd-sm pmd-accordion-icon-left">verified_user</i> Collapsible Group Item #3 </a> </h4>
		</div>
		<div id="collapseThree4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
			<div class="panel-body">Not every website needs an accordion menu and you certainly won’t find them all the time. But that’s no reason to ignore the concept entirely. The purpose of an accordion menu is to manage an overabundance of content through dynamic switching. Each interface works differently based on the circumstances of the layout. </div>
		</div>
	</div>
	<div class="panel panel-info"> 
		<div class="panel-heading" role="tab" id="headingFour">
			<h4 class="panel-title"><a  data-toggle="collapse" data-parent="#accordion4" href="#collapseFour4" aria-expanded="false" aria-controls="collapseFour4"  data-expandable="false"><i class="material-icons pmd-sm pmd-accordion-icon-left">account_box</i> Collapsible Group Item #4 </a> </h4>
		</div>
		<div id="collapseFour4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
			<div class="panel-body">So when exactly should you use accordions? Mostly with larger menus or content which might behave cleaner using expandable sections. These could be sub-headings or even multiple levels – the point is to organize content in a way that makes navigation simpler than endless scrolling. </div>
		</div>
	</div>
</div> <!--Accordion with colored icon end-->
    </div>
</div>


    <?php include('includes/footer.php');?>
        <script>
            $(document).ready(function(){
                $('.carousel[data-type="multi"] .item').each(function(){
                  var next = $(this).next();
                  if (!next.length) {
                    next = $(this).siblings(':first');
                  }
                  next.children(':first-child').clone().appendTo($(this));

                  for (var i=0;i<4;i++) {
                    next=next.next();
                    if (!next.length) {
                        next = $(this).siblings(':first');
                    }

                    next.children(':first-child').clone().appendTo($(this));
                  }
                });
            });
            $(window).on('load', function () {
                $('.ma5slider').ma5slider({
                    autoplayTime: 8e5

                });
            });
            $(document).ready(function () {
                $('.bxslider').bxSlider({
                    pager: false,
                    auto: true,
                    autoControls: true
                });
                $('.bxslider_carousel').bxSlider({
                      auto: true,
                      pager: false,
                    minSlides: 1,
                    maxSlides: 6,
                    moveSlides: 1,
                      slideWidth: 150,
                      slideMargin: 10
                });
            });
        </script>
        </body>

        </html>
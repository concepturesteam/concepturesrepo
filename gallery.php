<?php 
	$page_id=9;
	include('includes/header.php'); 
?>
<div class="inner_layout">
    <div class="about_company">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="contant">
                        <h1>Gallery</h1>
                        <h1 class="bodhi_color">BODHI CONCEPTEURS</h1>
                        <p> Bodhi Concepteurs consists of a dynamic, talented and experienced team of professionals experts at customizing architectural designs that stand out in the crowd leaving no barriers of half-baked premises.</p> </div>
                </div>
            </div>
        </div>
    </div>
    <div class="gallery_wrap inner_gallery">
    <div class="container">
        <div class="gallery_sec">
            <div class="demo-gallery">
            <ul id="lightgallery" class="list-unstyled row">
                <li class="col-xs-6 col-sm-4 col-md-4" data-responsive="images/2.jpg" data-src="images/2.jpg" data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                    <a href="">
                        <img class="img-responsive" src="images/2.jpg">
                    </a>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-4" data-src="images/3.jpg" data-sub-html="<h4>Bowness Bay</h4><p>A beautiful Sunrise this morning taken En-route to Keswick not one as planned but I'm extremely happy I was passing the right place at the right time....</p>">
                    <a href="">
                        <img class="img-responsive" src="images/3.jpg">
                    </a>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-4" data-src="images/best-office-interior-designer-08.jpg" data-sub-html="<h4>Bowness Bay</h4><p>A beautiful Sunrise this morning taken En-route to Keswick not one as planned but I'm extremely happy I was passing the right place at the right time....</p>">
                    <a href="">
                        <img class="img-responsive" src="images/best-office-interior-designer-08.jpg">
                    </a>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-4" data-responsive="images/2.jpg" data-src="images/2.jpg" data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                    <a href="">
                        <img class="img-responsive" src="images/2.jpg">
                    </a>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-4" data-src="images/3.jpg" data-sub-html="<h4>Bowness Bay</h4><p>A beautiful Sunrise this morning taken En-route to Keswick not one as planned but I'm extremely happy I was passing the right place at the right time....</p>">
                    <a href="">
                        <img class="img-responsive" src="images/3.jpg">
                    </a>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-4" data-src="images/best-office-interior-designer-08.jpg" data-sub-html="<h4>Bowness Bay</h4><p>A beautiful Sunrise this morning taken En-route to Keswick not one as planned but I'm extremely happy I was passing the right place at the right time....</p>">
                    <a href="">
                        <img class="img-responsive" src="images/best-office-interior-designer-08.jpg">
                    </a>
                </li>
            </ul>
        </div>
        </div>
    </div>
</div>
    
</div>


    <?php include('includes/footer.php');?>
        <script>
            $(document).ready(function(){
                $('.carousel[data-type="multi"] .item').each(function(){
                  var next = $(this).next();
                  if (!next.length) {
                    next = $(this).siblings(':first');
                  }
                  next.children(':first-child').clone().appendTo($(this));

                  for (var i=0;i<4;i++) {
                    next=next.next();
                    if (!next.length) {
                        next = $(this).siblings(':first');
                    }

                    next.children(':first-child').clone().appendTo($(this));
                  }
                });
                $("#lightgallery").lightGallery();
            });
            $(window).on('load', function () {
                $('.ma5slider').ma5slider({
                    autoplayTime: 8e5

                });
            });
            $(document).ready(function () {
                $('.bxslider').bxSlider({
                    pager: false,
                    auto: true,
                    autoControls: true
                });
                $('.bxslider_carousel').bxSlider({
                      auto: true,
                      pager: false,
                    minSlides: 1,
                    maxSlides: 6,
                    moveSlides: 1,
                      slideWidth: 150,
                      slideMargin: 10
                });
            });
        </script>
        <!-- Propeller tabs js -->
<script type="text/javascript" language="javascript" src="http://propeller.in/components/tab/js/tab-scrollable.js"></script>
<script type="text/javascript">
	$(document).ready( function() {
		$('.pmd-tabs').pmdTab();
	});
</script>
        </body>

        </html>
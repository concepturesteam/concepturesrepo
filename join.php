<?php 
	$page_id=11;
	include('includes/header.php'); 
?>
<div class="inner_layout">
   <!-- <div class="projects">
        <div class="project_slid">
            <ul class="bxslider">
                <li style="background:url('images/homePageBanner.png') center no-repeat; background-size:cover;">
                </li>
                <li style="background:url('images/pexels-photo_1.jpg') center no-repeat; background-size:cover;">
                </li>
                <li style="background:url('images/pexels-photo.jpg') center no-repeat; background-size:cover;">
                </li>
            </ul>
        </div>
    </div>-->
    <div class="about_company">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="contant">
                        <h1>Join with Us</h1>
                        <!--<h1 class="bodhi_color">BODHI CONCEPTEURS</h1>-->
                        <p> Bodhi Concepteurs consists of a dynamic, talented and experienced team of professionals experts at customizing architectural designs that stand out in the crowd leaving no barriers of half-baked premises.</p> </div>
                </div>
            </div>
        </div>
    </div>
    <div class="career_form">
        <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="contant">
                            <h1>Apply Now</h1><!--
                            <p>PLEASE PROVIDE SOME DETAILS TO GET AN ESTIMATE.</p>-->
                        </div>
                        <form>
                            <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                <label for="regular1" class="control-label"> Your Name* </label>
                                <input type="text" id="regular1" class="form-control"> </div>
                            <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                <label for="regular1" class="control-label"> Email*</label>
                                <input type="email" id="regular1" class="form-control"> </div>
                            <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                <label for="regular1" class="control-label"> Contact Number*</label>
                                <input type="text" id="regular1" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57'> </div>
                            <div class="form-group pmd-textfield pmd-textfield-floating-label">
<!--                                <label for="regular1" class="control-label"> Contact Number*</label>-->
                                <input type="file" id="regular1" class="form-control"> </div>
                            <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                <label for="regular1" class="control-label"> Message</label>
                                <textarea type="email" id="regular1" class="form-control"></textarea> </div>
                                <button type="submit" class="btn btn-lg pmd-btn-raised pmd-ripple-effect btn-primary">Send</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php include('includes/footer.php');?>
        <script>
            $(document).ready(function(){
                $('.carousel[data-type="multi"] .item').each(function(){
                  var next = $(this).next();
                  if (!next.length) {
                    next = $(this).siblings(':first');
                  }
                  next.children(':first-child').clone().appendTo($(this));

                  for (var i=0;i<4;i++) {
                    next=next.next();
                    if (!next.length) {
                        next = $(this).siblings(':first');
                    }

                    next.children(':first-child').clone().appendTo($(this));
                  }
                });
            });
            $(window).on('load', function () {
                $('.ma5slider').ma5slider({
                    autoplayTime: 8e5

                });
            });
            $(document).ready(function () {
                $('.bxslider').bxSlider({
                    pager: false,
                    auto: true,
                    autoControls: true
                });
                $('.bxslider_carousel').bxSlider({
                      auto: true,
                      pager: false,
                    minSlides: 1,
                    maxSlides: 6,
                    moveSlides: 1,
                      slideWidth: 150,
                      slideMargin: 10
                });
            });
        </script>
        </body>

        </html>
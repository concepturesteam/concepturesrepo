<?php 
	$page_id=1;
	include('includes/header.php'); 
?>
    <div class="about_company">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="company_slider">
                        <div class="wpb_wrapper">
                            <div class="prague-counter  alone_item">
                                <div class="figures triangle">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 200" preserveAspectRatio="xMidYMid meet">
                                        <polygon points="0 200, 0 0, 200,0"></polygon>
                                    </svg>
                                </div>
                                <div class="counter-outer s-back-switch" style="padding: 15px;     background: #659108;;"> <img src="images/0cb0bb46919687.586a967f3036d.jpg" alt="" class="prague-counter-img s-img-switch" style="display: none;">
                                    <div class="numbers">
                                        <svg width="100%" height="100%">
                                            <defs>
                                                <mask id="coming_mask_0" x="0" y="0">
                                                    <rect class="coming-alpha" x="0" y="0" width="100%" height="100%"></rect>
                                                    <text class="count number" x="50%" y="40%" text-anchor="middle" alignment-baseline="middle">WE</text>
                                                    <text class="count title" x="50%" y="75%" text-anchor="middle" alignment-baseline="middle">DEFINES QUALITY</text>
                                                </mask>
                                            </defs>
                                            <rect style="-webkit-mask: url(#coming_mask_0); mask: url(#coming_mask_0);" class="base" x="0" y="0" width="100%" height="100%"></rect>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="contant">
                        <!--                        <h1>This is the</h1>-->
                        <!--                        <h1 class="bodhi_color">We defines Quality</h1>-->
                        <p> Bodhi Concepteurs consists of a dynamic, talented and experienced team of professionals experts at customizing architectural designs that stand out in the crowd leaving no barriers of half-baked premises. With a badge of unprecedented legacy in serving the engineering and architecture industry for more than 5 years now, we have our base located in Calicut tending to a loyal base of conventional clientele all over the world.</p> <a href="about.php">Read more</a> </div>
                </div>
            </div>
        </div>
    </div>
    <div class="our_skills">
        <div class="prague-shortcode-parent ">
            <div class="prague-shortcode-parent-img s-back-switch" style="background-image: url(&quot;images/6fe48243174379.57e5b1bdd85ce-1024x372.jpg&quot;);"> <span class="overlay"></span> <img src="images/6fe48243174379.57e5b1bdd85ce-1024x372.jpg" class="s-img-switch" alt="" style="display: none;"> </div>
            <div class="prague-shortcode-content-wrapp">
                <div class="prague-shortcode-heading  light left">
                    <div class="parent-subtitle divider">SERVICES</div>
                    <h2 class="parent-title">This is what we do.</h2> </div>
                <div data-unique-key="0bf3c8aa9017e52dd041b7e2c3327621">
                    <div class="row prague_services prague_count_col3 prague_gap_col15 no-footer-content prague-load-wrapper" data-columns="prague_count_col3" data-gap="prague_gap_col15" style="position: relative; height: auto;     top: 130px;">
                        <div class="portfolio-item-wrapp prague_filter_class  p_f_f9e81a7" style="position: absolute; left: 0px; top: 0px;">
                            <div class="portfolio-item">
                                <div class="prague-services-wrapper"> <span class="services-item-icon icon-circle-compass"></span>
                                    <h2 class="services-item-title">Floor plan Analysis</h2>
                                    <div class="services-item-description">
                                        <p>We provide this services without charge. Bodhi Concepteurs is happy to assist you with your plan at any stage.</p>
                                    </div>
                                    <a href="services.php#plan" class="prague-services-link a-btn-2 creative anima"> <span class="a-btn-line"></span> READ </a>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-item-wrapp prague_filter_class  p_f_f9e81a7 column_paralax intrior_box_home" style="position: absolute; right: 0px; top: 0px;">
                            <div class="portfolio-item">
                                <div class="prague-services-wrapper"> <span class="services-item-icon icon-lightbulb"></span>
                                    <h2 class="services-item-title">Customised Interiors</h2>
                                    <div class="services-item-description">
                                        <p>We rely on our professional knowledge and advanced experience in the field to walk our client through every process </p>
                                    </div>
                                    <a href="services.php#interio" class="prague-services-link a-btn-2 creative anima"> <span class="a-btn-line"></span> READ </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--<div class="after_before">
        <div class="container">
            <h1>Facelift</h1>
            <div class="twentytwenty-container"> <img src="images/after_new.jpg" /> <img src="images/before_new.jpg" /> </div>
            <a href="#">View all</a>
            <a href="facelift.php" class="prague-services-link a-btn-2 creative anima"> <span class="a-btn-line"></span> View all </a>
        </div>
    </div>-->
<div class="after_before">
        <div class="container">
            <h1>Live Projects</h1>
            <ul class="bxslider">
                <li><img src="images/projects/IMG-20160613-WA0006.jpg" /></li>
                <li><img src="images/projects/IMG-20170517-WA0008.jpg" /></li><!--
                <li><img src="images/after_new.jpg" /></li>
                <li><img src="images/after_new.jpg" /></li>
                <li><img src="images/after_new.jpg" /></li>-->
            </ul>
            <a href="live-projects.php" class="prague-services-link a-btn-2 creative anima"> <span class="a-btn-line"></span> View all </a>
        </div>
    </div>
    <div class="projects">
        <div class="video-wrapper">
            <div class="center   small-video-row">
                <div>
                    <a href="bangalore_lumion.php">
                    <div class="video-overlay"> </div> <img src="images/1.jpg">
                    <div class="small-video-text">
                        <h2>Commercial</h2>
                        <h4>Bangalore</h4> </div></a>
                </div>
                <div>
                    <a href="chengottu_kav_masjid.php">
                    <div class="video-overlay"> </div> <img src="images/5.jpg">
                    <div class="small-video-text">
                        <h2>Masjid</h2>
                        <h4>Chengottu Kavu</h4> </div></a>
                </div>
                <div>
                    <a href="gafoor_home.php">
                    <div class="video-overlay"> </div> <img src="images/2.jpg">
                    <div class="small-video-text">
                        <h2>Gafoor</h2>
                        <h4>Thikkodi</h4> </div></a>
                </div>
                <div>
                    <a href="hameed_villa.php">
                    <div class="video-overlay"> </div> <img src="images/4.jpg">
                    <div class="small-video-text">
                        <h2>Hameed Villa</h2>
                        <h4></h4> </div></a>
                </div>
                <div>
                    <a href="pharmsy.php">
                    <div class="video-overlay"> </div> <img src="images/6.jpg">
                    <div class="small-video-text">
                        <h2>Pharmsi</h2>
                        <h4>Kozhikode</h4> </div></a>
                </div>
                <div>
                    <a href="sidco.php">
                    <div class="video-overlay"> </div> <img src="images/7.jpg">
                    <div class="small-video-text">
                        <h2>SIDCO</h2>
                        <h4>Vengara</h4> </div></a>
                </div>
            </div>
        </div>
    </div>
    <div class="clientele">
        <div class="container">
            <h1>Our Client Say</h1>
            <section id="carousel">
                <div class="quote"><i class="fa fa-quote-left fa-4x"></i></div>
                <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="6000">
                    <!-- Carousel indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#fade-quote-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#fade-quote-carousel" data-slide-to="1"></li>
                        <li data-target="#fade-quote-carousel" data-slide-to="2"></li>
                    </ol>
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <div class="active item">
                            <div class="profile-circle"><img src="images/logo-b.jpg" alt="Bodhi" /></div>
                            <h3>Kozhikode's</h3>
                            <blockquote>
                                <p>I am proud of the thoughtfulness, commitment and quality of work Concepteurs has put through the design, construction and commissioning of our organization facility. Teamwork is critical to any project; the Concepteurs team worked well together .</p>
                            </blockquote>
                        </div>
                        <div class="item">
                            <div class="profile-circle"><img src="images/logo-c.png" alt="Bodhi" /></div>
                            <h3>Royal Paris</h3>
                            <blockquote>
                                <p>Their project managers communicated well with my team and were responsive to our questions and concerns through the many changes along the way. They meet aggressive schedule while achieving our high quality and safety standards.The design team has always been attentive to customer requirements</p>
                            </blockquote>
                        </div>
                        <div class="item">
                            <div class="profile-circle"><img src="images/logo-d.jpg" alt="Bodhi" /></div>
                            <h3>Kerala SIDCO Ltd</h3>
                            <blockquote>
                                <p>They listened to our needs and  showed what great teamwork is all about.This stability, across both project management and engineering, saves us considerable time and effort.They have well-grounded people who work hard and understand our business. They helped us to overcome the various project challenges and completed the job on time and within budget.</p>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <?php include('includes/footer.php');?>
        <script>
            $(document).ready(function () {
                $('.carousel[data-type="multi"] .item').each(function () {
                    var next = $(this).next();
                    if (!next.length) {
                        next = $(this).siblings(':first');
                    }
                    next.children(':first-child').clone().appendTo($(this));
                    for (var i = 0; i < 4; i++) {
                        next = next.next();
                        if (!next.length) {
                            next = $(this).siblings(':first');
                        }
                        next.children(':first-child').clone().appendTo($(this));
                    }
                });
            });
            $(window).on('load', function () {
                $('.ma5slider').ma5slider({
                    autoplayTime: 8e5
                });
            });
            $(document).ready(function () {
                $('.bxslider').bxSlider({
                    pager: false
                    , auto: true
                    , autoControls: true
                });
                $('.bxslider_carousel').bxSlider({
                    auto: true
                    , pager: false
                    , controls: false
                    , minSlides: 1
                    , maxSlides: 6
                    , moveSlides: 1
                    , slideWidth: 150
                    , slideMargin: 40
                });
                $('.bxslider_offers').bxSlider({
                    auto: true
                    , pager: false
                    , controls: false
                    , minSlides: 1
                    , maxSlides: 3
                    , moveSlides: 1
                    , slideWidth: 350
                    , slideMargin: 10
                });
                $('.center').slick({
                    centerMode: true
                    , autoplay: true
                    , // cssEase: 'cubic-bezier(0.950, 0.050, 0.795, 0.035)',
                    easing: 'easeOutElastic'
                    , focusOnSelect: true
                    , centerPadding: '60px'
                    , slidesToShow: 3
                    , responsive: [{
                        breakpoint: 768
                        , settings: {
                            arrows: false
                            , centerMode: true
                            , slideWidth: 250
                            , centerPadding: '40px'
                            , slidesToShow: 3
                        }
                  }, {
                        breakpoint: 480
                        , settings: {
                            arrows: false
                            , centerMode: true
                            , centerPadding: '40px'
                            , slidesToShow: 1
                        }
                  }]
                });
            });
            $(window).load(function () {
                $(".twentytwenty-container[data-orientation!='vertical']").twentytwenty({
                    default_offset_pct: 0.7
                });
                $(".twentytwenty-container[data-orientation='vertical']").twentytwenty({
                    default_offset_pct: 0.3
                    , orientation: 'vertical'
                });
            });
        </script>
        </body>

        </html>
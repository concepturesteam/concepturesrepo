<?php 
	$page_id=3;
	include('includes/header.php'); 
?>
<div class="inner_layout">
    <div class="about_company">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="contant">
                        <h1>Services</h1>
                        <!--<h1 class="bodhi_color">BODHI CONCEPTEURS</h1>-->
                        <p> Bodhi Concepteurs consists of a dynamic, talented and experienced team of professionals experts at customizing architectural designs that stand out in the crowd leaving no barriers of half-baked premises.</p> </div>
                </div>
            </div>
        </div>
    </div>
    <div class="projects_section">
        <div class="container">
            
            <div class="row" id="plan">
                <div class="col-md-6 col-sm-8">
                    <div class="services_image">
                        <img src="images/88078a41049671.579736c352fcb.jpg" alt="">
                    </div>
                </div>
                <div class="col-md-6 col-sm-4">
                    <div class="portfolio-item">
                                    <div class="prague-services-wrapper" style="    padding-top: 0;"> <span class="services-item-icon icon-circle-compass"></span>
                                        <h2 class="services-item-title">Floor plan Analysis</h2>
                                        <div class="services-item-description">
                                            <p>We provide this services without charge. Bodhi Concepteurs is happy to assist you with your plan at any stage.<br><br>
                                            <b>How it works :</b><br>
                                                <ul>
                                                    <li>Ring us to meet our professionals</li>
                                                    <li>Send us your floor plan and contact address.</li>
                                                    <li>Let us know the scope of the work including the stage and requirement.</li>
                                                    <li>We assure you we will give a basic analysis report of your plan within 2 weeks,</li>
                                                </ul>
    <br>
    <b>Why Bodhi Concepteurs:</b><br>
    We design always with a concept that directly correlates to the needs of clients and your individual needs
                                            </p>
                                        </div>
                                        <!--<a href="#" class="prague-services-link a-btn-2 creative anima"> <span class="a-btn-line"></span> READ </a>-->
                                    </div>
                                </div>
                </div>
            </div>
            <div class="row" id="interio">
                    <div class="col-md-6 col-sm-8 col-md-pull-6 col-sm-push-6">
                        <div class="services_image">
                            <img src="images/after.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-4 col-md-pull-6 col-sm-pull-8">
                        <div class="portfolio-item">
                                <div class="prague-services-wrapper"> <span class="services-item-icon icon-lightbulb"></span>
                                    <h2 class="services-item-title">Customised Interiors</h2>
                                    <div class="services-item-description">
                                        <p>Our interior environments are all custom solutions, planned to maximize operational efficiency through space layout and technology. We deliver imaginative work environments, amenities and visual imagery that attract the best talent. Our online proprietary tools helps you to customize almost every aspect of your home. You can choose from the wide range of furniture models we have and can install within 45 days of booking.</p>
                                    </div>
                                    <a href="intirio/index.php" class="prague-services-link a-btn-2 creative anima"> <span class="a-btn-line"></span> READ </a>
                                </div>
                            </div>
                    </div>
                </div>
            <div class="row">
                    <div class="col-sm-12">
                        <div class="portfolio-item">
                                <div class="prague-services-wrapper"><!-- <span class="services-item-icon icon-lightbulb"></span>-->
                                    <h2 class="services-item-title">Technological</h2>
                                    <div class="services-item-description">
                                        <p>Technological innovations are seeping into the industry so thoroughly that even the humble hard hat is changing, with high-tech smart hats coming to the market, alongside new advances that benefit every area of the industry. Through technology we make you more efficient and safer. </p>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h4>Security :</h4>
                                                <ul>
                                                    <li>Home application</li>

                                                       <li> Cctv camera</li>

                                                       <li> Video door phone</li>

                                                      <li>  Security alarm</li>

                                                      <li>  Intelligent lock</li>

                                                      <li>  Human body sensors</li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6">
                                                <h4>Convenience</h4>
                                                <ul>
                                                    <li>Water purifier</li>
                                                      <li>  Solar panel</li>
                                                     <li>   Automatic doors and gates</li>
                                                      <li>  Lifts</li>
                                                </ul>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
        <div class="row">
                    <div class="col-sm-12">
                        <div class="portfolio-item">
                                <div class="prague-services-wrapper"><!-- <span class="services-item-icon icon-lightbulb"></span>-->
                                    <h2 class="services-item-title">Quality</h2>
                                    <div class="services-item-description">
                                        <p>It is not just in the sourcing of best priced, its about how we work from design to workmanship.Our commitment and involvement are consistently strong through every phase of the design and construction process.  Our methodology have been praised by many owners and operators and we are constantly improving and perfecting our system.  </p>
                                    </div>
                                </div>
                            </div>
                    </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                        <div class="portfolio-item">
                                <div class="prague-services-wrapper"><!-- <span class="services-item-icon icon-lightbulb"></span>-->
                                    <h2 class="services-item-title">Service Strategy</h2>
                                    <div class="services-item-description">
                                        <p>A pleasurable experience from beginning to end. Our goal is to understand our clients needs and to create each and delivers beyond expectation. Listening to the client and distilling that knowledge into an approach which is then filtered through our knowledge and experience is essential to achieving our goal. Whether the aesthetic is more contemporary, modern, or traditional, we are fully engaged and informed in our work.</p>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
        </div>
    </div>
    <!--<div class="skills_wrap">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="contant">
                        <h1>Skills</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
    


    <?php include('includes/footer.php');?>
        <script>
            $(document).ready(function(){
                $('.carousel[data-type="multi"] .item').each(function(){
                  var next = $(this).next();
                  if (!next.length) {
                    next = $(this).siblings(':first');
                  }
                  next.children(':first-child').clone().appendTo($(this));

                  for (var i=0;i<4;i++) {
                    next=next.next();
                    if (!next.length) {
                        next = $(this).siblings(':first');
                    }

                    next.children(':first-child').clone().appendTo($(this));
                  }
                });
            });
            $(window).on('load', function () {
                $('.ma5slider').ma5slider({
                    autoplayTime: 8e5

                });
            });
            $(document).ready(function () {
                $('.bxslider').bxSlider({
                    pager: false,
                    auto: true,
                    autoControls: true
                });
                $('.bxslider_carousel').bxSlider({
                      auto: true,
                      pager: false,
                    minSlides: 1,
                    maxSlides: 6,
                    moveSlides: 1,
                      slideWidth: 150,
                      slideMargin: 10
                });
            });
        </script>
        </body>

        </html>
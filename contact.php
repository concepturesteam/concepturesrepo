<?php 
	$page_id=12;
	include('includes/header.php'); 
?>
    <div class="inner_layout contact_wrap">
        <div id="map"></div>
            <div class="contact_form">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="adrss_details">
                            <h1>OFFICE ADDRESS</h1>
                            <p>3rd Floor, CD Tower
                            Opp. Baby Memorial Hospital
                            Calicut, Kerala</p>
                            <span>Mob : <a href="#">+91 7560940007</a></span>
                            <span>E-Mail : <a href="#">mail@designsbodhi.com</a></span>
                            <div class="social_media"> <a href="#"><span><i class="fa fa-instagram" aria-hidden="true"></i></span></a> <a href="#"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a> <a href="#"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a> <a href="#"><span><i class="fa fa-linkedin" aria-hidden="true"></i></span></a> </div>
                            <div class="google_map">
                                <div class='pin'></div>
                                <div class='pulse'></div>
                                <span data-target="#simple-dialog_12" data-toggle="modal">View in Map</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 touch_with">
                        <div class="contant">
                            <h1>Touch with Us</h1>
<!--                            <p>PLEASE PROVIDE SOME DETAILS TO GET AN ESTIMATE.</p>-->
                        </div>
                        <form>
                            <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                <label for="regular1" class="control-label"> Your Name* </label>
                                <input type="text" id="regular1" class="form-control"> </div>
                            <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                <label for="regular1" class="control-label"> Contact Number*</label>
                                <input type="text" id="regular1" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57'> </div>
                            <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                <label for="regular1" class="control-label"> Email*</label>
                                <input type="email" id="regular1" class="form-control"> </div>
                            <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                <label for="regular1" class="control-label"> Message</label>
                                <textarea type="email" id="regular1" class="form-control"></textarea> </div>
                                <button type="submit" class="btn btn-lg pmd-btn-raised pmd-ripple-effect btn-primary">Send</button>
                        </form>
                    </div>
                </div>
            </div>
    </div>

<div tabindex="-1" class="modal fade" id="simple-dialog_12" style="display: none;" aria-hidden="true">
                <div class="modal-dialog">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <div class="modal-content">
                        <div class="modal-body">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3913.0070706286115!2d75.79208364769902!3d11.26088969686583!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba6594fae4dbd41%3A0xe8b8601c5d7155b4!2sCD+TOWER%2C+Mini+Bypass+Rd%2C+Thiruthiyad%2C+Kozhikode%2C+Kerala+673004!5e0!3m2!1sen!2sin!4v1492580035837" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
    <!--center: {lat: 11.261264, lng: 75.792895}-->
    <?php include('includes/footer.php');?>
        <script>
            function initMap() {
                // Styles a map in night mode.
                var map = new google.maps.Map(document.getElementById('map'), {
                    center: {
                        lat: 11.261264
                        , lng: 75.792895
                    }
                    , zoom: 16,
                    scrollwheel: false
                    ,styles: [
            {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
            {
              featureType: 'administrative.locality',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'poi',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'geometry',
              stylers: [{color: '#263c3f'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'labels.text.fill',
              stylers: [{color: '#6b9a76'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry',
              stylers: [{color: '#38414e'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry.stroke',
              stylers: [{color: '#212a37'}]
            },
            {
              featureType: 'road',
              elementType: 'labels.text.fill',
              stylers: [{color: '#9ca5b3'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry',
              stylers: [{color: '#746855'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry.stroke',
              stylers: [{color: '#1f2835'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'labels.text.fill',
              stylers: [{color: '#f3d19c'}]
            },
            {
              featureType: 'transit',
              elementType: 'geometry',
              stylers: [{color: '#2f3948'}]
            },
            {
              featureType: 'transit.station',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'water',
              elementType: 'geometry',
              stylers: [{color: '#17263c'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.fill',
              stylers: [{color: '#515c6d'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.stroke',
              stylers: [{color: '#17263c'}]
            }
          ]
                });
            }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_tcJPg13HszqKO7FX1hp5sQIE_otU9Ik&callback=initMap" async defer></script>
        </body>

        </html>